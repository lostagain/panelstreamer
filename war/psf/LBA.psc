<Collection name="A test of the format">
<layer name="Base" zIndex="1000">

<panel ID="1" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="0" LocZ="0"></panel>
<panel ID="2" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="932" LocY="0" LocZ="0"></panel>
<panel ID="3" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="0" LocZ="0"></panel>
<panel ID="4" Data="lbaimages/_Quality_/Citadel_island_full_1x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="0" LocZ="0"></panel>
<panel ID="5" Data="lbaimages/_Quality_/Citadel_island_full_1x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="0" LocZ="0"></panel>
<panel ID="6" Data="lbaimages/_Quality_/Citadel_island_full_1x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="0" LocZ="0"></panel>

<panel ID="11" Data="lbaimages/_Quality_/Citadel_island_full_2x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="512" LocZ="0"></panel>
<panel ID="12" Data="lbaimages/_Quality_/Citadel_island_full_2x2.jpg" SizeX="932" SizeY="516" LocX="932" LocY="512" LocZ="0"></panel>
<panel ID="13" Data="lbaimages/_Quality_/Citadel_island_full_2x3.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="512" LocZ="0"></panel>
<panel ID="14" Data="lbaimages/_Quality_/Citadel_island_full_2x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="512" LocZ="0"></panel>
<panel ID="15" Data="lbaimages/_Quality_/Citadel_island_full_2x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="512" LocZ="0"></panel>
<panel ID="16" Data="lbaimages/_Quality_/Citadel_island_full_2x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="512" LocZ="0"></panel>


<panel ID="21" Data="lbaimages/_Quality_/Citadel_island_full_3x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="1024" LocZ="0"></panel>
<panel ID="22" Data="lbaimages/_Quality_/Citadel_island_full_3x2.jpg" SizeX="932" SizeY="516" LocX="932" LocY="1024" LocZ="0"></panel>
<panel ID="23" Data="lbaimages/_Quality_/Citadel_island_full_3x3.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="1024" LocZ="0"></panel>
<panel ID="24" Data="lbaimages/_Quality_/Citadel_island_full_3x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="1024" LocZ="0"></panel>
<panel ID="25" Data="lbaimages/_Quality_/Citadel_island_full_3x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="1024" LocZ="0"></panel>
<panel ID="26" Data="lbaimages/_Quality_/Citadel_island_full_3x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="1024" LocZ="0"></panel>

<panel ID="31" Data="lbaimages/_Quality_/Citadel_island_full_4x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="1536" LocZ="0"></panel>
<panel ID="32" Data="lbaimages/_Quality_/Citadel_island_full_4x2.jpg" SizeX="932" SizeY="516" LocX="932" LocY="1536" LocZ="0"></panel>
<panel ID="33" Data="lbaimages/_Quality_/Citadel_island_full_4x3.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="1536" LocZ="0"></panel>
<panel ID="34" Data="lbaimages/_Quality_/Citadel_island_full_4x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="1536" LocZ="0"></panel>
<panel ID="35" Data="lbaimages/_Quality_/Citadel_island_full_4x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="1536" LocZ="0"></panel>
<panel ID="36" Data="lbaimages/_Quality_/Citadel_island_full_4x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="1536" LocZ="0"></panel>

<panel ID="41" Data="lbaimages/_Quality_/Citadel_island_full_5x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="2048" LocZ="0"></panel>
<panel ID="42" Data="lbaimages/_Quality_/Citadel_island_full_5x2.jpg" SizeX="932" SizeY="516" LocX="932" LocY="2048" LocZ="0"></panel>
<panel ID="43" Data="lbaimages/_Quality_/Citadel_island_full_5x3.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="2048" LocZ="0"></panel>
<panel ID="44" Data="lbaimages/_Quality_/Citadel_island_full_5x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="2048" LocZ="0"></panel>
<panel ID="45" Data="lbaimages/_Quality_/Citadel_island_full_5x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="2048" LocZ="0"></panel>
<panel ID="46" Data="lbaimages/_Quality_/Citadel_island_full_5x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="2048" LocZ="0"></panel>


<panel ID="51" Data="lbaimages/_Quality_/Citadel_island_full_6x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="2560" LocZ="0"></panel>
<panel ID="52" Data="lbaimages/_Quality_/Citadel_island_full_6x2.jpg" SizeX="932" SizeY="516" LocX="932" LocY="2560" LocZ="0"></panel>
<panel ID="53" Data="lbaimages/_Quality_/Citadel_island_full_6x3.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="2560" LocZ="0"></panel>
<panel ID="54" Data="lbaimages/_Quality_/Citadel_island_full_6x4.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="2560" LocZ="0"></panel>
<panel ID="55" Data="lbaimages/_Quality_/Citadel_island_full_6x5.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="2560" LocZ="0"></panel>
<panel ID="56" Data="lbaimages/_Quality_/Citadel_island_full_6x6.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="2560" LocZ="0"></panel>


<panel ID="61" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="0" LocY="30820" LocZ="0"></panel>
<panel ID="62" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="932" LocY="3082" LocZ="0"></panel>
<panel ID="63" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="1864" LocY="3082" LocZ="0"></panel>
<panel ID="64" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="2796" LocY="3082" LocZ="0"></panel>
<panel ID="65" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="3728" LocY="3082" LocZ="0"></panel>
<panel ID="66" Data="lbaimages/_Quality_/Citadel_island_full_1x1.jpg" SizeX="932" SizeY="516" LocX="4660" LocY="3082" LocZ="0"></panel>

</layer>

</Collection>