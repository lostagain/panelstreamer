A project to make a infinite canvas engine usable in all browsers, as well as help design a standard xml like format for infinite canvas creations, whether it be for comics or other uses. A (crude) in-development demo is here; http://www.darkflame.co.uk//panalstreamer/panelstreamer.html

For more information on the infinite canvas concept, please read "Reinventing Comics" by Scott McCloud.