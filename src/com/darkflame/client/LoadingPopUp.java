package com.darkflame.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/** tells the user the file is loading, and when enough is loaded gives an option to continue **/



public class LoadingPopUp extends DecoratedPopupPanel {
	
	
	SpiffyLoadingIcon LoadingIcon = new SpiffyLoadingIcon(false);

	Button Continue = new Button("Continue");
	Label Status = new Label("Loading...");

	VerticalPanel container = new VerticalPanel();
	
	LoadingPopUp thisPopup = this;
	
	public LoadingPopUp(){
		
	//	this.setWidth("450px");
	//	this.setHeight("150px");
		
		Continue.setEnabled(false);
		
		container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		container.add(Status);
		container.add(LoadingIcon);
		container.add(Continue);
		
		this.setWidget(container);
		this.setModal(true);
		this.setGlassEnabled(true);
		
	
		Continue.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				thisPopup.hide();
			}
			
		});
	}
	
	
	public void stepClockForward(){
		LoadingIcon.stepClockForward();
	}
	
	public void setTotalUnitsToLoad(int setTotalUnitsToLoad){
		LoadingIcon.setTotalUnits(setTotalUnitsToLoad);
	}
	
	public void startLoading(){
		LoadingIcon.startAnimation();
	}
	
	public void stopLoading(){
		LoadingIcon.stopAnimation();
		Continue.setEnabled(true);
		Status.setText("Loaded");
		this.setGlassEnabled(false);
		this.setModal(true);
	}
	public void enableContinue(){
		Continue.setEnabled(true);
		Status.setText("Cacheing...");
	}
	@Override
	public void onAttach(){
		LoadingIcon.reset();
		Continue.setEnabled(false);
		Status.setText("Loading...");
		this.setModal(true);
		this.setGlassEnabled(true);
		super.onAttach();
	}
	
	@Override
	public void hide(){
		stopLoading();
		super.hide();
	}
}
