package com.darkflame.client;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;


public class Preloader {
	
	static Logger Log = Logger.getLogger("Preloader");

private static PostLoadAction PostAction;

	final static ArrayList<String> HtmlItems = new ArrayList<String>();
	final static ArrayList<String> HtmlFileNames = new ArrayList<String>();
	
	
	final static ArrayList<String> LoadingList = new ArrayList<String>();
	/** purely for debugging, can be removed**/
	final static ArrayList<String> CurrentlyLoadingList = new ArrayList<String>();
	
	
	final static ArrayList<Image> LoadingWidgets = new ArrayList<Image>();
	
	final static HashMap<String,Runnable> PostCommands = new HashMap<String,Runnable>();
	
	static LoadHandler OnLoadHandler;
	static ErrorHandler OnErrorHandler;
//	final static Image LoadThis = new Image();
	
	static final int MAX_SIMULTANIOUS_LOADING = 6;
	
	
	/** preloads game images. Simply use "addToLoading" then "preload" when everything you want
	 * is added. New images are triggered to load after each one is finished. **/
	public Preloader(){
		/*
		//split up to array
		List<String> LoadingList = Arrays.asList(  LoadingString.split("\n")  );
		

		System.out.print("preloading..."+LoadingList.size());
		
		ArrayList<String> LoadingArrayList =new ArrayList<String>(LoadingList); 

		System.out.print("preloading..."+LoadingArrayList.size());
				
		Iterator<String> it = (LoadingList.iterator());
		
		//loop over
		while (it.hasNext()) {

			
			//for each image, we preload			
		String preloadthis = it.next().trim();
	
		if (preloadthis.toLowerCase().endsWith("jpg")){
			PreloadImage(preloadthis);	
		}
		
		if (preloadthis.toLowerCase().endsWith("png")){
			PreloadImage(preloadthis);
		}
		if (preloadthis.toLowerCase().endsWith("gif")){
			PreloadImage(preloadthis);
		}
		}
		
		*/
		
		
		setupHandlers();
		
		
	}

	private static void setupHandlers() {
		
		OnLoadHandler = new LoadHandler(){
			
			
			public void onLoad(LoadEvent event) {
				Image LoadThis = (Image) event.getSource();
				
				String currentlyLoading=LoadThis.getUrl();
				
				Log.info("Just Loaded:"+currentlyLoading);
				
				
				CurrentlyLoadingList.remove(currentlyLoading);
				
				panelstreamer.LoadingPopup.stepClockForward();
				
				//add details to stats
				XYPoint sizeXY = new XYPoint(LoadThis.getOffsetWidth(),LoadThis.getOffsetHeight());
				
				CollectionStats.ImageData.put(LoadThis.getUrl(),sizeXY );
				
				//Log.info("-=-Loaded:"+currentlyLoading );
				
				// remove from list
				//LoadingList.remove(0);
				//if (LoadingList.remove(currentlyLoading)){
				//	Log.info("-=-removed:"+currentlyLoading );
				//} else {
				//	Log.info("-=-failed to remove:"+currentlyLoading );
				//}
				
				Runnable Commands=PostCommands.get(currentlyLoading);
				if (Commands!=null){
					Commands.run();					
				}
				
				
				if (LoadingList.size()==0){

					if (PostAction!=null){
					PostAction.ImagesAllLoaded();
					}
					
					return;
				}
				
				//load the next;		
				
				String requestedURL=LoadingList.get(0);
								
						
				Log.info("-=-Loading next:"+requestedURL);
				LoadThis.setUrl(requestedURL);		
				LoadingList.remove(requestedURL);
				
				//if (JAM.Quality.equalsIgnoreCase("debug")){
					
				//Log.info("Left To Start Loading:"+LoadingList.toString());		
				//}
				CurrentlyLoadingList.add(LoadThis.getUrl());
				
				//if (JAM.Quality.equalsIgnoreCase("debug")){
					
				//Log.info("In the process of Loading:"+CurrentlyLoadingList.toString());	
				//}
				
				//convert url to absolute and replace references
				//LoadingList.set(LoadingList.indexOf(requestedURL), LoadThis.getUrl());
				PostCommands.put(LoadThis.getUrl(), PostCommands.get(requestedURL));
			}
			
		};
		
		

		OnErrorHandler = new ErrorHandler(){

			@Override
			public void onError(ErrorEvent event) {
				Image LoadThis = (Image) event.getSource();
				String currentlyLoading=LoadThis.getUrl();
				
				Log.info("Just Failed to Load:"+currentlyLoading);	
				CurrentlyLoadingList.remove(currentlyLoading);
				// remove from list
			//	LoadingList.remove(currentlyLoading);
				
				//add details to stats
				XYPoint sizeXY = new XYPoint(LoadThis.getOffsetWidth(),LoadThis.getOffsetHeight());
				
				CollectionStats.ImageData.put(LoadThis.getUrl(),sizeXY );
				
				//temp				
				panelstreamer.LoadingPopup.stepClockForward();
				
				Runnable Commands=PostCommands.get(currentlyLoading);
				
				
				
				if (Commands!=null){					

							
					Commands.run();
					
				}
				if (LoadingList.size()==0){					

					if (PostAction!=null){
					PostAction.ImagesAllLoaded();
					}
					
					return;
				}
				
				//load the next;
				
				
				String requestedURL=LoadingList.get(0);
				Log.info("-=-Started Loading this after error:"+requestedURL);
				
				LoadThis.setUrl(requestedURL);	
				LoadingList.remove(requestedURL);
				
				//if (JAM.Quality.equalsIgnoreCase("debug")){
				//Log.info("Left To Start Loading:"+LoadingList.toString());
				//}
				
					

				CurrentlyLoadingList.add(LoadThis.getUrl());
				Log.info("In the process of Loading:"+CurrentlyLoadingList.toString());	
				
				
				//convert url to absolute and replace references
				//LoadingList.set(LoadingList.indexOf(requestedURL), LoadThis.getUrl());
				PostCommands.put(LoadThis.getUrl(), PostCommands.get(requestedURL));
			}
			
		};
	}
	
	/** returns true if successfully added, false if not**/
	static public boolean addToLoading(String URL){
		
		//maybe LoadingList could be a "TreeSet" so duplicates arnt allowed without needing a check
				
		if (!LoadingList.contains(URL)){
			LoadingList.add(URL);
			return true;
		}
		
		return false;
		//PreloadImage(GWT.getHostPageBaseURL()+URL);
		
		//Window.alert("<br>-=-add image to preload"+URL );
	}
	/** returns true if successfully added, false if not**/
	static public boolean addToLoading(String URL, Runnable doThisWhenLoaded){
		
		//maybe LoadingList could be a "TreeSet" so duplicates arnt allowed without needing a check
		
		if (!LoadingList.contains(URL)){
			LoadingList.add(URL);
			PostCommands.put(URL,doThisWhenLoaded);
			return true;
		}
		return false;
		
		
	}
	
	
	public static void preloadList(){
		
		if (CurrentlyLoadingList.size()>0){
			
			Log.info("Still have "+CurrentlyLoadingList.size()+" things left to load");
			Log.info("They are:"+CurrentlyLoadingList.toString());
			return;
		}
		
		setupHandlers();
		
		
		
		//This loops over the preload list
		//When each entry is loaded, its removed from the list
		//and the next item loaded.
		Log.info("-loading images:" );
		
		if (LoadingList.size()<1){
			Log.info("no images to load" );
		}
		
		int i=0;
		
		while (i<MAX_SIMULTANIOUS_LOADING) {
			
			i++;
			
		//	Window.alert("<br>preloading list");
			
		Image LoadThis = new Image();
		
		LoadThis.addLoadHandler(OnLoadHandler);
		LoadThis.addErrorHandler(OnErrorHandler);
		
		RootPanel.get().add(LoadThis, -12000, -12000);
		
		LoadThis.getElement().setId("_PRELOADERPANEL_");
		LoadThis.setStylePrimaryName("hiddenImagePanel");
		LoadingWidgets.add(LoadThis);
		
		if (LoadingList.size()==0){
			
			Log.info("Nothing left in loading list to start loading");
						
			break;
		}
		
		String requestedURL = LoadingList.get(0);
		
		LoadingList.remove(requestedURL);
		LoadThis.setUrl(requestedURL);
		
		//convert url to absolute and replace references
		//LoadingList.set(LoadingList.indexOf(requestedURL), LoadThis.getUrl());		
		PostCommands.put(LoadThis.getUrl(), PostCommands.get(requestedURL));
		Log.info("-=-loading:" + requestedURL);
	}
		
//		
//		final Image LoadThis2 = new Image();
//		LoadThis2.addLoadHandler(OnLoadHandler);		
//		LoadThis2.addErrorHandler(OnErrorHandler);
//		RootPanel.get().add(LoadThis2,-12000,-12000);		
//		LoadThis2.setStylePrimaryName("hiddenImagePanel");
//		
//		final String requestedURL2=LoadingList.get(0);	
//		LoadingList.remove(requestedURL2);
//		
//		LoadThis2.setUrl(requestedURL2);
//		//convert url to absolute and replace references
//		//LoadingList.set(LoadingList.indexOf(requestedURL), LoadThis.getUrl());		
//		PostCommands.put(LoadThis2.getUrl(), PostCommands.get(requestedURL2));				
//		Log.info("-=-loading:"+requestedURL2 );
		

	}

	/** this should be run once all loading is complete */
	private static void tidyOurMess() {
	//
		
		
		//detach images
		for (Image imageToRemove : LoadingWidgets) {
			imageToRemove.removeFromParent();
		}
		
		
		LoadingWidgets.clear();
	}
	
	
	
	
	
	
	
	/**prefetches an image right now using the Image.prefetch funtion */
	public static void PreloadImage(String URL){
		//base image
		
		Image.prefetch(URL);
		//get route name
		
		//String filename = URL.substring(URL.lastIndexOf("/")+1);
		//String loc = URL.substring(0, URL.lastIndexOf("/")+1);

	//	JAM.DebugWindow.addText("preloaded..."+URL);		

		
	}
	
	
	public void AddItem(String newitem, String newname){
		HtmlItems.add(newitem);
		HtmlFileNames.add(newname);
	}
	public void RemoveItem(String removethisitem){
		//find location
		int i=0;
		int indextoremove = -1;
		for (Iterator<String>it = HtmlFileNames.iterator(); it.hasNext(); ) {
			  String currentItem = it.next(); 
			  if (currentItem.compareTo(removethisitem)==0){
				  indextoremove=i;
			  };
			  i=i+1;
			
		}
		//if present
		if (indextoremove>-1){
		HtmlItems.remove(indextoremove);
		HtmlFileNames.remove(indextoremove);
		}
	}
	public String GetItem(String ItemName){
		String Item = "";
		//find location
		int i=0;
		int itemindex = -1;
		for (Iterator<String>it = HtmlFileNames.iterator(); it.hasNext(); ) {
			  String currentItem = it.next(); 
			  if (currentItem.compareTo(ItemName)==0){
				  itemindex=i;
			  };
			  i=i+1;
			
		}
		//if present
		if (itemindex>-1){
			Item = HtmlItems.get(itemindex);
		} else {
			Item = "";
		}
			
		
		return Item;
	}
	
	
	 public interface PostLoadAction {
		  
         public void ImagesAllLoaded();
 
  }
	 
	static public void setPostAction(PostLoadAction meep){
	    Log.info("PostLoad action set");
	    PostAction=meep;
}
	
	
}
