package com.darkflame.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

import com.darkflame.client.ComicPanel.PanelTypes;
import com.darkflame.client.PanelInfo.PositionInPanel;
import com.darkflame.client.Preloader.PostLoadAction;
import com.darkflame.client.panelContentTypes.ThreadPanelContent;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasMouseDownHandlers;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;

/** https://code.google.com/p/panelstreamer/<br>
 * A project to make a infinite canvas engine usable in all browsers, <br>
 * as well as help design a standard xml like format for infinite canvas creations, <br>
 * whether it be for comics or other uses.<br>
 *  <br>
 * A (crude) in-development demo is here; <br>
 * http://www.darkflame.co.uk//panalstreamer/panelstreamer.html<br>
 * <br>
 * For more information on the infinite canvas concept, please read <br>
 * "Reinventing Comics" by Scott McCloud?.<br>
 * <br>
 */
public class panelstreamer implements EntryPoint, ValueChangeHandler<String> {

	// debugging
	static Logger Log = Logger.getLogger("Panelstreamer");
	
	//loading icon
//	static SpiffyLoadingIcon LoadingIcon = new SpiffyLoadingIcon();
	static LoadingPopUp LoadingPopup = new LoadingPopUp();
	
	// current loaded collection
	static String CurrentPanelCollectionName = "";

	// global information stores for panels
	//
	/** Actually stores the panels **/
	final static GlobalPanelStore AllComicPanels = new GlobalPanelStore();
	/** stores the information for each panel **/
	final static PanelInfoStore AllComicPanelsInfo = new PanelInfoStore();
	/**
	 * stores the information for each panel, but only for panels that are
	 * loaded
	 **/
	final static PanelInfoStore AllLoadedComicPanelsInfo = new PanelInfoStore();
	// -----

	// all the layers
	static ArrayList<ComicPanelLayer> layers = new ArrayList<ComicPanelLayer>();
	// all the info for the layers (very basic size,zIndex,name)
	static ArrayList<layerInfo> layersinfo = new ArrayList<layerInfo>();

	// (not used anymore)
	// final static AbsolutePanel LayerPanel = new AbsolutePanel();

	static int ContainerSizeX = 90; // umm..probably wrong
	static int ContainerSizeY = 80;

	static int MaxCanvasInternalHeight = 0;
	static int MaxCanvasInternalWidth = 0;

	static dragableAbsolutePanel ContainerPanel = new dragableAbsolutePanel(
			ContainerSizeX, ContainerSizeY);

	// final static ArrayList<PanelInfo> LayersPanelsInfo = new
	// ArrayList<PanelInfo>();
	// final static ArrayList<ArrayList<PanelInfo>> AllLayersPanelInfo = new
	// ArrayList<ArrayList<PanelInfo>>();

	static double GlobalZoom = 100;
	static double ChangeInZoom = 0;

	static int currentXpos = 0;
	static int currentYpos = 0;

	// box for manual file url selection
	final AskForURLPopupPanel RequestURL = new AskForURLPopupPanel();

	// Range square to help debug the loading (not that usefull really)
	// static SpiffyLine loadingHelper = new SpiffyLine(300,300,10,12);
	static Image loadingHelperImage = new Image("images/HelperFrame.png");
	static ComicPanel LoadingHelper = new ComicPanel(loadingHelperImage,
			ComicPanel.PanelTypes.IMAGE, 9999,
			(int) (2000 * (GlobalZoom / 100)),
			(int) (2000 * (GlobalZoom / 100)), 1, "", false);

	static SpiffyListBox SelectFile = new SpiffyListBox();

	// editbox
	final static EditFileBox EditBox = new EditFileBox();
//edit panel details
	final static PanelInfoPanel infoPanel = new PanelInfoPanel();
	
	private static String CurrentFile;

	// image sizes locations
	// these strings are simply swapped in the image URLs based on current zoom
	// level
	// (could be replaced by a full LOD system in future by having these levels
	// defined in the file)
	final static String LOW_QUALITY_LOCATION = "low";
	final static String MEDIUM_QUALITY_LOCATION = "medium";
	final static String HIGH_QUALITY_LOCATION = "high";

	// current quality (set to default
	static String currentQuality = HIGH_QUALITY_LOCATION;
	//

	// main control bar
	static final controllbar MainControlBar = new controllbar();
	
	//Bottom bar
	static final BottomBar bottomBar = new BottomBar();
	

	// private int count;
	public static boolean UpdatePositionAfterLoad = false;
	public static int StartAtX = 0;
	public static int StartAtY = 0;
	public static double StartAtZoom = 0;

	// Variables to do with editing
	public static Boolean EDITMODE = false;
	public static ComicPanel PanelBeingEdited = null;
	public static boolean PanelBeingMoved = false;

	public void onModuleLoad() {

		SpeedTracerlog("start set up");

		loadingHelperImage.addMouseDownHandler(new MouseDownHandler() {
			public void onMouseDown(MouseDownEvent event) {
				event.preventDefault();
			}

		});

		RootPanel.get("TopBar").add(MainControlBar);
		

		//bottomBar floats
		int y = Window.getClientHeight();
		RootPanel.get().add(bottomBar, 10, y-50);
		
		setupFileSelect();

		// add listener
		History.addValueChangeHandler(this);

		// check history
		String historyString = History.getToken();

		if (historyString.indexOf("File=") > -1) {

			SpeedTracerlog("updating based on history");
			updateBasedOnHistoryToken(historyString);

		}

	}

	/** for speed tracer testing/debugging **/
	public static native void SpeedTracerlog(String msg) /*-{
		var logger = $wnd.console;
		if (logger && logger.markTimeline) {
			logger.markTimeline(msg);
		}
	}-*/;

	/**
	 * gets a ini file from the server that lists psc files for the user to
	 * select
	 **/
	private void setupFileSelect() {

		RequestBuilder getAvaliableLayers = new RequestBuilder(
				RequestBuilder.GET, "psf/layers.ini");
		try {
			getAvaliableLayers.sendRequest("", new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					Window.alert("no  layers found at location");
					return;
				}

				public void onResponseReceived(Request request,
						Response response) {

					String layerCollectionData = response.getText();
					Log.info("layer Data=" + layerCollectionData);

					if (layerCollectionData.contains(",")) {
						// split for each url
						String layerCollectionLocations[] = layerCollectionData
								.split(",");

						for (int collectionNum = 0; collectionNum < layerCollectionLocations.length; collectionNum++) {
							SelectFile
									.addItem(layerCollectionLocations[collectionNum]);
						}
					} else {
						// one option only
						SelectFile.addItem(layerCollectionData);
					}

					SelectFile.addItem("Custom (Select URL)");
					SelectFile.setWidth("300px");
					SelectFile.setVisibleItemCount(9);
					SelectFile.addChangeHandler(new ChangeHandler() {
						public void onChange(ChangeEvent event) {

							Window.setTitle("before selected file");

							// get current selected
							String Selected = SelectFile.getItemText(SelectFile
									.getSelectedIndex());

							if (Selected
									.equalsIgnoreCase("Custom (Select URL)")) {

								Window.setTitle("custom selected");

								RequestURL.show();
								RequestURL.center();
								RequestURL.getElement().getStyle().setProperty(
										"zIndex", "99999");

								Window.setTitle("popuploaded");

							} else {

								// set the title
								MainControlBar.setTitle(Selected);
								ContainerPanel.reset();
								loadPSCFile(Selected, false);

							}
							// UpdatePosition(200,200);

						}
					});

					MainControlBar.addFileSelectList(SelectFile);

				}
			});
		} catch (RequestException e) {
			e.printStackTrace();
			Window.alert("error loading file");
			return;
		}

	}

	private void updateBasedOnHistoryToken(String historyString) {

		// if theres a new file
		if (historyString.indexOf("File=") > -1) {

			Log.info("file=" + historyString);
			// get filename
			String fileToLoad = historyString.substring(historyString
					.indexOf("File=") + 5, historyString.indexOf("&amp;"));

			// get co-ordinates
			// Window.alert("-  ("+(historyString.indexOf("X=")+2)+")");
			// Window.alert("-  ("+(historyString.indexOf("&=")+2)+")");
			int Xend = historyString.indexOf("&amp;", (historyString
					.indexOf("X=") + 2));
			Xend = (Xend < 0) ? historyString.length() : Xend;
			Log.info("x end=" + Xend);
			// get filename
			String Xs = historyString.substring(
					historyString.indexOf("X=") + 2, Xend);

			int Yend = historyString.indexOf("&amp;", (historyString
					.indexOf("Y=") + 2));
			Yend = (Yend < 0) ? historyString.length() : Yend;
			Log.info("y end=" + Yend);
			String Ys = historyString.substring(
					historyString.indexOf("Y=") + 2, Yend);

			Log.info("x= " + Xs + " Y=" + Ys);

			String Zooms = historyString.substring(historyString
					.indexOf("Zoom=") + 5);

			int X = Integer.parseInt(Xs);
			int Y = Integer.parseInt(Ys);
			double Zoom = Double.parseDouble(Zooms);

			Log.info("zoom = " + Zoom);

			// update its position after loading flags;
			UpdatePositionAfterLoad = true;
			StartAtX = X;
			StartAtY = Y;
			StartAtZoom = Zoom;

			// load it
			loadPSCFile(fileToLoad, false);

			// else if theres just a position
		} else if (historyString.indexOf("X=") > -1) {
			// cancel current movements

			ContainerPanel.disableAllMovement();

			Log.info("..............set location ...");
			int Xend = historyString.indexOf("&",
					(historyString.indexOf("X=") + 2));
			Xend = (Xend < 0) ? historyString.length() : Xend;

			// get filename
			String Xs = historyString.substring(
					historyString.indexOf("X=") + 2, Xend);

			int Yend = historyString.indexOf("&",
					(historyString.indexOf("Y=") + 2));
			Yend = (Yend < 0) ? historyString.length() : Yend;

			String Ys = historyString.substring(
					historyString.indexOf("Y=") + 2, Yend);
			Log.info(".x=" + Xs + ".Y=" + Ys);

			// String
			// Xs=historyString.substring(historyString.indexOf("X=")+2,historyString.indexOf("&amp;",(historyString.indexOf("X=")+2)));
			// String
			// Ys=historyString.substring(historyString.indexOf("Y=")+2,historyString.indexOf("&amp;",(historyString.indexOf("Y=")+2)));
			// String
			// Zooms=historyString.substring(historyString.indexOf("Zoom=")+5);

			UpdatePosition(Integer.parseInt(Xs), Integer.parseInt(Ys));
			ContainerPanel.setPosition(Integer.parseInt(Xs), Integer
					.parseInt(Ys));

			ContainerPanel.enableAllMovement();

		}

	}

	public static void loadXMLdata(final String URL, final boolean AddToList,
			String XMLdata) {
		// parse it
		Document Layers = XMLParser.parse(XMLdata);
		
		Log.info("file data=..." + XMLdata);

		// get collection name
		String collectionName = Layers.getLastChild().getAttributes()
				.getNamedItem("name").getNodeValue();
		Log.info("collection is called: " + collectionName);
		CurrentPanelCollectionName = collectionName;

		// set title as name
		MainControlBar.FileListTitle.setText(collectionName);
		// --

		// Loop for each element into array
		NodeList layerlist = Layers.getElementsByTagName("layer");

		System.out.print("\n Number Of Layers " + layerlist.getLength());

		// loop for each layer
		int currentlayernum = 0;
		while (currentlayernum < layerlist.getLength()) {
			System.out.print("\n Adding panels to layer number "
					+ currentlayernum + " \n");

			NodeList panellist = layerlist.item(currentlayernum)
					.getChildNodes();

			// get layer info
			String LayerName = layerlist.item(currentlayernum).getAttributes()
					.getNamedItem("name").getNodeValue();

			String LayerSizeX = "";
			String LayerSizeY = "";
			String LayerStatic = "";
			try {
				LayerSizeX = layerlist.item(currentlayernum).getAttributes()
						.getNamedItem("sizeX").getNodeValue();
				LayerSizeY = layerlist.item(currentlayernum).getAttributes()
						.getNamedItem("sizeY").getNodeValue();
			} catch (Exception e) {
				Log.info("size not set");
			}
			try {
				Log.info("is layer  static?..");
				LayerStatic = layerlist.item(currentlayernum).getAttributes()
						.getNamedItem("LayerStatic").getNodeValue();
			} catch (Exception e) {
				Log.info("layer not static");
				// layer not static
			}
		//	int LayerZDepth = Integer.parseInt(layerlist.item(currentlayernum)
		//			.getAttributes().getNamedItem("zIndex").getNodeValue());

			System.out.print("\n Layer Info =" + LayerName + " " 
					+ "\n");

			ArrayList<PanelInfo> newlayer = new ArrayList<PanelInfo>();

			// loop for each panel
			int currentpaneln = 0;
			while (currentpaneln < panellist.getLength()) {
				Node currentpanel = panellist.item(currentpaneln);

				// if panel
				if (currentpanel.getNodeName().trim().compareTo("panel") == 0) {
					// load data of panel

					int ID = Integer.parseInt(currentpanel.getAttributes()
							.getNamedItem("ID").getNodeValue());
					
					String DataContents = currentpanel.getAttributes()
							.getNamedItem("Data").getNodeValue();

					// if Data is a thread, we get the location and size from
					// its source and destination
					int SizeX = 100;
					int SizeY = 100;
					int LocX = 5;
					int LocY = 5;
					int LocZ = 5;
					ComicPanel.PanelTypes Type = PanelTypes.IMAGE;
					String DrawFrom = "";
					String DrawTo = "";
					String DrawColour = "";
					String LinkURL = null;
					String TextData = "";
					
					if (DataContents.startsWith("Thread")) {
						System.out.print("\n loading thread data");
						// data is all loaded later with threads (panels have to
						// load into info first;
						SizeX = 100;
						SizeY = 100;
						LocX = 5;
						LocY = 5;
						LocZ = 5;
						Type = PanelTypes.THREAD;

						//if ends with ; remove
						if (DataContents.endsWith(";")){
							DataContents = DataContents.substring(0, DataContents.length()-1);
							
						}
						
						DrawColour = DataContents.substring(DataContents
								.indexOf("Col:") + 4, DataContents
								.indexOf(";FromID"));

						DrawFrom = DataContents.substring(DataContents
								.indexOf("FromID:") + 7, DataContents
								.indexOf(";ToID"));

						DrawTo = DataContents.substring(DataContents
								.indexOf(";ToID:") + 6);

					} else {

						SizeX = Integer.parseInt(currentpanel.getAttributes()
								.getNamedItem("SizeX").getNodeValue());
						SizeY = Integer.parseInt(currentpanel.getAttributes()
								.getNamedItem("SizeY").getNodeValue());
						LocX = Integer.parseInt(currentpanel.getAttributes()
								.getNamedItem("LocX").getNodeValue());
						LocY = Integer.parseInt(currentpanel.getAttributes()
								.getNamedItem("LocY").getNodeValue());
						LocZ = Integer.parseInt(currentpanel.getAttributes()
								.getNamedItem("LocZ").getNodeValue());
						//if its text
						if (DataContents.startsWith("TextBox")) {
							TextData = DataContents;
							Type = PanelTypes.TEXTBOX;
						}
						if (DataContents.endsWith(".html")) {
							Type = PanelTypes.WEBPAGE;
						}
						if (DataContents.toLowerCase().endsWith("jpg")
								|| DataContents.toLowerCase().endsWith("png")){
								
								String imageURL = DataContents.replace(
										"_Quality_", currentQuality);
								
							Type = PanelTypes.IMAGE;

							//preload
							Preloader.addToLoading(imageURL);
							
						}
						
						// if theres a link
						if (currentpanel.getAttributes()
								.getNamedItem("LinkURL") != null) {
							Log.info("link found, parseing..");
							LinkURL = currentpanel.getAttributes()
									.getNamedItem("LinkURL").getNodeValue();
						}
					}

					//temp; check its a real url in future
					String url = DataContents;
					// add data to array
					
					PanelInfo newinfo = new PanelInfo(ID, Type, url,TextData,
							LocX, LocY, LocZ, SizeX, SizeY, LinkURL,
							currentlayernum);

					// add extra data if its a thread
					if (newinfo.Type == PanelTypes.THREAD) {
						//newinfo.DrawFrom = DrawFrom;
						//newinfo.DrawTo = DrawTo;
						//newinfo.DrawColour = DrawColour;
						try {
							newinfo.setThreadDataByStrings(DrawFrom, DrawTo, DrawColour);
						} catch (PositionInPanel.UnrecognisedPositionError e) {
							Log.info("UnrecognisedPositionError in thread, skipping|"+e.PositionNotRecognised);
							//ERROR in thread, dont create
							currentpaneln++;
							continue;
						}
						
					}

					newlayer.add(newinfo);

					// update max size the IC will need
					if (newinfo.LocationX + newinfo.SizeX > MaxCanvasInternalWidth) {
						Log.info("Max X updated" + newinfo.LocationX
								+ newinfo.SizeX);
						MaxCanvasInternalWidth = newinfo.LocationX
								+ newinfo.SizeX;
					}
					if (newinfo.LocationY + newinfo.SizeY > MaxCanvasInternalHeight) {
						MaxCanvasInternalHeight = newinfo.LocationY
								+ newinfo.SizeY;
						Log.info("Max Y updated" + newinfo.LocationY
								+ newinfo.SizeY);

					}
					// ---

					Log.info("\n AddedPanel: "
							+ newlayer.get(newlayer.size() - 1).ID + " "
							+ DataContents + " " + SizeX + " " + SizeY + " "
							+ LocX + " " + LocY + " " + LocZ );

					// add every panel info to this store too;
					AllComicPanelsInfo.add(newinfo);
					
					//add to stats
					CollectionStats.AllIDs.add(newinfo.ID);
					if (newinfo.Type == PanelTypes.IMAGE){
						
					    CollectionStats.ImageURLs.add(newinfo.urlData);
					
					} else if (newinfo.Type == PanelTypes.WEBPAGE){
					CollectionStats.WebpageURLs.add(newinfo.urlData );
					}
					

				}

				currentpaneln++;
			}
			
			LoadingPopup.setTotalUnitsToLoad(Preloader.LoadingList.size());
			
			//start the image preloader
			Preloader.setPostAction(new PostLoadAction(){
				@Override
				public void ImagesAllLoaded() {
					Log.info("______________all images loaded");
					LoadingPopup.hide();
					
					
				}
				
			});
			Preloader.preloadList();
			
			// add the newly made layer;
			// AllLayersPanelInfo.add(newlayer);
			layerInfo newlayerinfo = new layerInfo(LayerName);

			Log.info("setting layer info:" + LayerName);

			if ((LayerSizeX.length() > 1) && (LayerSizeY.length() > 1)) {
				newlayerinfo.SizeX = (int) (Integer.parseInt(LayerSizeX));
				newlayerinfo.SizeY = (int) (Integer.parseInt(LayerSizeY));
			}
			if (LayerStatic.equals("true")) {
				// Log.info("layer static2");
				newlayerinfo.isStatic = true;
			} else {
				// Log.info("layer not static2");
				newlayerinfo.isStatic = false;
			}
			layersinfo.add(newlayerinfo);

			currentlayernum++;
		}

		// now we have to load the thread data (as this needs to wait till all
		// over data is loaded, so it can work out where to put them;
		// extract data needed for panel info;

		// Iterator<PanelInfo> panels = AllComicPanelsInfo.AllPanels.iterator();
		Iterator<PanelInfo> panels = AllComicPanelsInfo.getIterator();

		while (panels.hasNext()) {

			PanelInfo cp = panels.next();
			if (cp.urlData.toLowerCase().startsWith("thread=")) {

				cp.Type = PanelTypes.THREAD;

				System.out.print("\n -thread data");
				String threaddata = cp.urlData.substring(7);
				String Colour = threaddata.substring(
						threaddata.indexOf("Col:") + 4, threaddata.indexOf(";",
								threaddata.indexOf("Col:")));

				//int FromID = Integer.parseInt(threaddata.substring(threaddata
				//		.indexOf("FromID:") + 7, threaddata.indexOf(";",
				//		threaddata.indexOf("FromID:"))));
				//NEW IMPROVED:
				int FromID = cp.DrawFrom.PanelID;
				
				
			//	String Loc = threaddata.substring(
				//		threaddata.indexOf("Loc:") + 4, threaddata.indexOf(";",
				//				threaddata.indexOf("Loc:")));
				//NEW IMPROVED:
				String Loc = cp.DrawFrom.PositionInPanel.toString();
				
				//int ToID = Integer.parseInt(threaddata.substring(threaddata
				//		.indexOf("ToID:") + 5, threaddata.indexOf(";",
					//	threaddata.indexOf("ToID:"))));
				//NEW IMPROVED:
				int ToID = cp.DrawTo.PanelID;
				
				//String Dest = threaddata.substring(
				//		threaddata.indexOf("Dest:") + 5, threaddata.indexOf(
						//		";", threaddata.indexOf("Dest:")));
				//NEW IMPROVED:
				String Dest = cp.DrawTo.PositionInPanel.toString();
				
				//System.out.print("\n From=" + FromID + ": Loc=" + Loc + ": To "
				//		+ ToID + ": Dest=" + Dest);

				// now we work out the co-ordinates to use for this panel
				cp.LocationX = AllComicPanelsInfo.GetByID(FromID)
						.getXLocationByString(Loc);
				cp.LocationY = AllComicPanelsInfo.GetByID(FromID)
						.getYLocationByString(Loc);
				// and the size by the position of the destination
				int destX = AllComicPanelsInfo.GetByID(ToID)
						.getXLocationByString(Dest.trim());
				int destY = AllComicPanelsInfo.GetByID(ToID)
						.getYLocationByString(Dest.trim());

				cp.SizeX = Math.abs(cp.LocationX - destX)
						+ (int) (6 * (panelstreamer.GlobalZoom / 100)); // line
				// thickness;
				cp.SizeY = Math.abs(cp.LocationY - destY)
						+ (int) (6 * (panelstreamer.GlobalZoom / 100)); // line
				// thickness

				// change the data to the format required for line.
				if (destY < cp.LocationY) {
					// move up and change thread origin
					cp.LocationY = destY;
					cp.urlData = Colour + ",10,4";

				}
				if (destY > cp.LocationY) {
					// move up and change thread origin
					cp.LocationY = AllComicPanelsInfo.GetByID(FromID)
							.getYLocationByString(Loc);
					cp.urlData = Colour + ",6,12";

				}

				System.out.print("\n Loc=" + cp.LocationX + " " + cp.LocationY);
				System.out.print("\n Size=" + cp.SizeX + " " + cp.SizeY);
				System.out.print("\n direction..." + cp.urlData.split(",")[0]);

			}

		}

		// Now we can load the layers!
		// Load each layer from files specified in the LayersPanelsInfo

		// add to list if successful
		if (AddToList) {
			SelectFile.addItem(URL);
		}

		// loadEverythingOntoLayers();

		Log.info("creating layers");
		createLayersNeeded();
	}

	public static void LoadDataIntoArray(String filename,
			Boolean AddToListOnSuccess) {

		final String URL = filename;
		final boolean AddToList = AddToListOnSuccess;

		// Get file
		RequestBuilder parselayerfile = new RequestBuilder(RequestBuilder.GET,
				filename);

		try {
			parselayerfile.sendRequest("", new RequestCallback() {

				public void onError(Request request, Throwable exception) {

					Window.alert("no psc file found at location");
					return;

				}

				public void onResponseReceived(Request request,
						Response response) {

					String XMLdata = response.getText();

					loadXMLdata(URL, AddToList, XMLdata);

				}
			});
		} catch (RequestException e) {
			e.printStackTrace();

			Window.alert("error loading file");
			return;
		}

	}

	public static void loadThesePanels(ArrayList<PanelInfo> listToLoad) {

		Log.info("(load these panels)");

		Iterator<PanelInfo> LoadThesePanels = listToLoad.iterator();

		while (LoadThesePanels.hasNext()) {

			PanelInfo CurrentPanelToLoadIno = LoadThesePanels.next();

			// make sure its not there already!
			if (AllLoadedComicPanelsInfo.IDexists(CurrentPanelToLoadIno.ID)) {
				// if it is we just skip it
				continue;
			}

			ComicPanel newpanel = null;
			Widget panelContents = null;
			
			ComicPanel.PanelTypes PanelType = null;

			Log.info("(adding panel-" + CurrentPanelToLoadIno.ID);

			// work out the new zindex

			final int ZINDEX = CurrentPanelToLoadIno.LocationZ; // zindex
			
			Log.info("(at depth-" + ZINDEX);
			// if its a standard image panel
			if (CurrentPanelToLoadIno.urlData.toLowerCase().endsWith("jpg")
					|| CurrentPanelToLoadIno.urlData.toLowerCase().endsWith("png")) {
				
				String imageURL = CurrentPanelToLoadIno.urlData.replace(
						"_Quality_", currentQuality);
				
				panelContents = new Image(imageURL);
				
				
				// stop dragging in firefox?
				((HasMouseDownHandlers) panelContents)
						.addMouseDownHandler(new MouseDownHandler() {
							public void onMouseDown(MouseDownEvent event) {
								event.preventDefault();
							}
						});

				PanelType = ComicPanel.PanelTypes.IMAGE;
				/*
				 * newpanel = new ComicPanel( panalContents,
				 * CurrentPanelToLoadIno.Type, CurrentPanelToLoadIno.ID, (int)
				 * (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)), (int)
				 * (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)), ZINDEX,
				 * CurrentPanelToLoadIno.LinkTo);
				 */
				// debug.setText(" loaded : "+newpanelimage.getUrl()+" ");

			}

			// if its a webpage
			if (CurrentPanelToLoadIno.urlData.toLowerCase().endsWith("html")
					|| CurrentPanelToLoadIno.urlData.toLowerCase()
							.endsWith("HTML")) {

				panelContents = new Frame(CurrentPanelToLoadIno.urlData);

				PanelType = ComicPanel.PanelTypes.WEBPAGE;
				/*
				 * newpanel = new ComicPanel( panalContents,
				 * CurrentPanelToLoadIno.Type, CurrentPanelToLoadIno.ID, (int)
				 * (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)), (int)
				 * (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)), ZINDEX,
				 * CurrentPanelToLoadIno.LinkTo);
				 */

				// debug.setText(" loaded : "+panalContents.getUrl()+" ");

				// AllComicPanels.add(newpanel);
				// AllLoadedComicPanelsInfo.add(CurrentPanelToLoadIno);
				// layers.get(CurrentPanelToLoadIno.onLayerNumber).add(newpanel,
				// (int)(CurrentPanelToLoadIno.LocationX*
				// (GlobalZoom/100)),(int)(CurrentPanelToLoadIno.LocationY*
				// (GlobalZoom/100)));
				// System.out.print("\n -added: "+((Image)newpanel.contents).getUrl()+" at "+CurrentPanelToLoadIno.LocationX+","+(int)(CurrentPanelToLoadIno.LocationY*
				// (GlobalZoom/100))+" as ID="+CurrentPanelToLoadIno.ID);

			}

			// if its text
			if (CurrentPanelToLoadIno.urlData.startsWith("Text")) {

				// this will need to be changed for other text types if they are
				// ever used

				CurrentPanelToLoadIno.Type = PanelTypes.TEXTBOX;
				// CurrentPanelToLoadIno.Type = CurrentPanelToLoadIno.Data
				// .substring(0, CurrentPanelToLoadIno.Data.indexOf("="))
				// .toLowerCase();

				Log.info("\n -added text : " + CurrentPanelToLoadIno.Type);

				HTML text = new HTML(CurrentPanelToLoadIno.urlData
						.substring(CurrentPanelToLoadIno.urlData.indexOf("=") + 1));
			//	text.setHTML(text.getHTML().replaceAll("\\\\n", "<br>"));

				//VerticalPanel textcontainer = new VerticalPanel();
			//	textcontainer.add(text);
			//	textcontainer.setCellVerticalAlignment(text,
			//			VerticalPanel.ALIGN_MIDDLE);

			//	text.setHorizontalAlignment(Label.ALIGN_CENTER);

				TextPanelContent textcontainer = new TextPanelContent(text);
				
				panelContents = textcontainer;
				PanelType = ComicPanel.PanelTypes.TEXTBOX;
				/*
				 * newpanel = new ComicPanel( panalContents,
				 * CurrentPanelToLoadIno.Type, CurrentPanelToLoadIno.ID, (int)
				 * (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)), (int)
				 * (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)), ZINDEX,
				 * CurrentPanelToLoadIno.LinkTo);
				 */

				// newpanel.setBorders(true);

				// System.out.print(" loaded : "+text.getText()
				// +" at "+CurrentPanelToLoadIno.LocationX);

				// AllComicPanels.add(newpanel);
				// AllLoadedComicPanelsInfo.add(CurrentPanelToLoadIno);
				// layers.get(CurrentPanelToLoadIno.onLayerNumber).add(newpanel,
				// (int)(CurrentPanelToLoadIno.LocationX*
				// (GlobalZoom/100)),(int)(CurrentPanelToLoadIno.LocationY*
				// (GlobalZoom/100)));
				// System.out.print("\n -added: "+((Image)newpanel.contents).getUrl()+" at "+CurrentPanelToLoadIno.LocationX+","+(int)(CurrentPanelToLoadIno.LocationY*
				// (GlobalZoom/100))+" as ID="+CurrentPanelToLoadIno.ID);

			}

			// if its thread;
			if (CurrentPanelToLoadIno.Type == PanelTypes.THREAD) {

				// rather then an image we make a spiffy line object, the start
				// and end point depending on targets

				ThreadPanelContent thread = new ThreadPanelContent(
						(int) (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)),
						(int) (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)),
						CurrentPanelToLoadIno.urlData.split(",")[0],
						Integer
								.parseInt(CurrentPanelToLoadIno.urlData.split(",")[1]),
						Integer
								.parseInt(CurrentPanelToLoadIno.urlData.split(",")[2]));
				panelContents = thread;
				PanelType = ComicPanel.PanelTypes.THREAD;

				/*
				 * newpanel = new ComicPanel( panalContents,
				 * CurrentPanelToLoadIno.Type, CurrentPanelToLoadIno.ID, (int)
				 * (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)), (int)
				 * (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)), ZINDEX,
				 * CurrentPanelToLoadIno.LinkTo);
				 */

				// newpanel.PanalType = "thread";

				Log
						.info("\n -adding: thread size y:"
								+ (int) (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)));
				Log
						.info("\n -adding: thread loc y:"
								+ (int) (CurrentPanelToLoadIno.LocationY * (GlobalZoom / 100)));

			}

			// create panel
			//newpanel = new ComicPanel(panelContents, PanelType,
		//			CurrentPanelToLoadIno.ID,
			//		(int) (CurrentPanelToLoadIno.SizeX * (GlobalZoom / 100)),
		//			(int) (CurrentPanelToLoadIno.SizeY * (GlobalZoom / 100)),
			//		ZINDEX, CurrentPanelToLoadIno.LinkTo);
			
			newpanel = new ComicPanel(panelContents, PanelType,
					CurrentPanelToLoadIno.ID,
					(int) (CurrentPanelToLoadIno.SizeX),
					(int) (CurrentPanelToLoadIno.SizeY),
					ZINDEX, CurrentPanelToLoadIno.LinkTo);
			
			if (PanelType==PanelTypes.TEXTBOX) {
				newpanel.setBorders(true);
			}
			//hide is layer if off
			//if (layers.get(CurrentPanelToLoadIno.onLayerNumber).isVisible==false){
			//	newpanel.setVisible(false);				
			//}

			AllComicPanels.add(newpanel);
			AllLoadedComicPanelsInfo.add(CurrentPanelToLoadIno);

			Log.info("(added panel to loaded panels)");

			// layers.get(CurrentPanelToLoadIno.onLayerNumber).add(newpanel,
			// (int)(CurrentPanelToLoadIno.LocationX*
			// (GlobalZoom/100)),(int)(CurrentPanelToLoadIno.LocationY*
			// (GlobalZoom/100)));

			Log.info("_________________adding the panel to display;");
			ContainerPanel
					.addPanel(
							newpanel,
							(int) (CurrentPanelToLoadIno.LocationX * (GlobalZoom / 100)),
							(int) (CurrentPanelToLoadIno.LocationY * (GlobalZoom / 100)));

			Log.info("adding the panel "+CurrentPanelToLoadIno.ID+" to layer ("+CurrentPanelToLoadIno.onLayerNumber+");");
			
			layers.get(CurrentPanelToLoadIno.onLayerNumber).addPanel(newpanel);

			// System.out.print("\n -added: "+((Image)newpanel.contents).getUrl()+" at "+CurrentPanelToLoadIno.LocationX+","+(int)(CurrentPanelToLoadIno.LocationY*
			// (GlobalZoom/100))+" as ID="+CurrentPanelToLoadIno.ID);

			//Log.info("(making link)");
			// create a click listener
			if (CurrentPanelToLoadIno.LinkTo != null) {
				
				Log.info("(adding action)");
				
				String link = CurrentPanelToLoadIno.LinkTo;
				newpanel.setLinkToAction(link);
				

				/*
				 * newpanel.addClickHandler(new ClickHandler(){
				 * 
				 * public void onClick(ClickEvent event) {
				 * Log.info("(click detected)");
				 * 
				 * ComicPanel senderPanel = (ComicPanel)event.getSource();
				 * String link = senderPanel.linkUrl; // if set to edit
				 * 
				 * // else it does its default action //ComicPanel triggerPanel
				 * = (ComicPanel)event.getSource();
				 * 
				 * //Window.alert(triggerPanel.IDNumber +"bo!"+link); if
				 * ((link.toLowerCase
				 * ().startsWith("http:"))||(link.toLowerCase()
				 * .startsWith("www."))){
				 * 
				 * Window.open(link, "_blank", "");
				 * 
				 * 
				 * } else {
				 * 
				 * Log.info("set history");
				 * 
				 * History.newItem(link,true);
				 * 
				 * 
				 * }
				 * 
				 * } });
				 */

			}

		}

	}

	public static void createLayersNeeded() {

		// loop for all layers in file
		int CurrentLayerNum = 0;

		while (CurrentLayerNum < layersinfo.size()) {
			// while (CurrentLayerNum < AllLayersPanelInfo.size()) {

			// add name 
			//int zdepth = layersinfo.get(CurrentLayerNum).zDepth;

			ComicPanelLayer CurrentLayer = new ComicPanelLayer(layersinfo
					.get(CurrentLayerNum).Title);

			// DOM.setStyleAttribute(CurrentLayer.getElement(),
			// "zIndex",zdepth+"");

			CurrentLayerNum++;
			Log.info("adding layer ");
			layers.add(CurrentLayer);
		}

		Log.info("set up layers");

		// ----

		if (ContainerPanel.isAttached() == false) {
			RootPanel.get("InfiniteCanvas").add(ContainerPanel);
			// set size
			ContainerPanel.setSize("100%", "100%");
		}


		ContainerPanel.reset();

		// set size
		ContainerPanel.updateMaxSize(MaxCanvasInternalWidth,
				MaxCanvasInternalHeight);

		// add helper to dap
		// ContainerPanel.dragableLayer.add(LoadingHelper, 0, 0);

		// add the layer option box
		controllbar.LayerList.setLayerList(layersinfo);

		MainControlBar.addLayerList();
		RootPanel.get().add(infoPanel, Window.getClientWidth()-480 , MainControlBar.getOffsetHeight()-30);
		
		infoPanel.refreshBoxs();
		
		// now we update the position to default if no custom position is set;
		if (UpdatePositionAfterLoad == false) {
			UpdatePosition(200, 200);
		} else {

			Log.info("updateing position to = " + GlobalZoom + ":" + StartAtX
					+ " " + StartAtY);

			GlobalZoom = StartAtZoom;
			Log.info("position updated0");
			UpdatePosition(StartAtX, StartAtY);

			Log.info("position updated1");
			ContainerPanel.setPosition(StartAtX, StartAtY);

			Log.info("position updated2");

		}

	}

	/**
	 * Loads a PanelStreamCollection, if the flag is set, it will add the url to
	 * the list on success
	 **/
	static void loadPSCFile(String Selected, Boolean AddURLToListOnSuccess) {
		
		// add directory prefix if not starting with http
				if (!(Selected.startsWith("http"))) {
					Selected = GWT.getHostPageBaseURL() + "psf/" + Selected;

				}

		Log.info("loading file: " + Selected);
				
		// display loading icon
		LoadingPopup.center();
		
		//Window.alert("starting to load: " + Selected);
		
		// disable motion
		ContainerPanel.disableAllMovement();

		// clear data
		clearExistingData();

		// display loading widget
		//SpiffyLoadingIcon loader = new SpiffyLoadingIcon();
		//RootPanel.get().add(loader, 200, 200);

		// load
		LoadDataIntoArray(Selected, AddURLToListOnSuccess);

		CurrentFile = Selected;

		//Window.setTitle("after array loading");

		// re-enable movement

		ContainerPanel.enableAllMovement();
		
		//enable people to skip preload
		LoadingPopup.enableContinue();

	}

	public static void clearExistingData() {
		// clear
		layers.clear();
		layersinfo.clear();

		// if container has stuff already,we clear it
		/*
		 * if (ContainerPanel != null ){
		 * 
		 * ContainerPanel.inner.clear(); ContainerPanel.Container.clear();
		 * ContainerPanel.clear();
		 * 
		 * }
		 */
		MaxCanvasInternalHeight = 0;
		MaxCanvasInternalWidth = 0;

		AllLoadedComicPanelsInfo.clear();

		// (not used anymore)
		// LayerPanel.clear();

		// LayersPanelsInfo.clear();
		// AllLayersPanelInfo.clear();

		AllComicPanels.clear();
		AllComicPanelsInfo.clear();

		
		//clear the stats
		CollectionStats.clearStats(); 
		
		ChangeInZoom = 0;
		GlobalZoom = 100;

		currentXpos = 0;
		currentYpos = 0;
	}

	static public void UpdatePosition(int X, int Y) {

		// update position feedback on screen
		currentXpos = X;
		currentYpos = Y;

		// update token
		History.newItem("File=" + CurrentFile + "&amp;X=" + X + "&amp;Y=" + Y
				+ "&amp;Zoom=" + GlobalZoom, false);

		MainControlBar.setCurrentPositionLabel(X, Y);

		// only if movement is false
		if (!ContainerPanel.isMoving) {
			// layers.get(0).setWidgetPosition(LoadingHelper,currentXpos-(int)
			// Math.round(1000 * (GlobalZoom/100)),currentYpos-(int)
			// Math.round(1000 * (GlobalZoom/100)));
			// ContainerPanel.dragableLayer.setWidgetPosition(LoadingHelper,
			// currentXpos - (int) Math.round(1000 * (GlobalZoom / 100)),
			// currentYpos - (int) Math.round(1000 * (GlobalZoom / 100)));

		}

		// heres where the magic happens.

		// first we remove stuff outside twice the loading range (loading
		Log.info("test log0");
		removePanelsOutOfRange(currentXpos, currentYpos, 1000);

		Log.info("position updateing 1- X=" + X + "Y=" + Y);

		// then we add stuff within the range thats not there already;
		// range is scaled inversely based on zoom (smaller=quicker loading!)
		addPanelsWithin(currentXpos, currentYpos,
				(int) (1000 * (100 / GlobalZoom)));

		Log.info("position updateing 2-");
		// then we add the nearest things to the preload queue;

	}

	public static void addPanelsWithin(int X, int Y, int Range) {

		// first we divide the range by the current Zoom level.
		// Range = (int) (Range * (GlobalZoom/100));
		// System.out.print("\n Loading panels within: "+Range+" each way");

		// loop over all panels looking for those within range to add.
		// Iterator<PanelInfo> panels = AllComicPanelsInfo.AllPanels.iterator();
		Iterator<PanelInfo> panels = AllComicPanelsInfo.getIterator();

		ArrayList<PanelInfo> LoadThesePanels = new ArrayList<PanelInfo>();

		// location and range has to be scaled
		X = (int) (X / (GlobalZoom / 100));
		Y = (int) (Y / (GlobalZoom / 100));
		// Range = (int) (Range * (GlobalZoom/100));
		Log.info("Loading panels within: " + Range + " each way of " + X);

		Log.info("(adding panels)");

		while (panels.hasNext()) {
			PanelInfo paneltotest = panels.next();

			if ((paneltotest.LocationX < (X + Range))
					&& (((paneltotest.LocationX + paneltotest.SizeX) > (X - Range)))) {
				if ((paneltotest.LocationY < (Y + Range))
						&& (((paneltotest.LocationY + paneltotest.SizeY) > (Y - Range)))) {

					// If not already loaded!

					LoadThesePanels.add(paneltotest);
				}
			}

		}

		// load them
		loadThesePanels(LoadThesePanels);

	}

	public static void removePanelsOutOfRange(int X, int Y, int Range) {
		ArrayList<ComicPanel> RemoveList = new ArrayList<ComicPanel>();

		// loop over all panels looking for those within range to add.
		// Iterator<PanelInfo> panels = AllComicPanelsInfo.AllPanels.iterator();
		Iterator<PanelInfo> panels = AllComicPanelsInfo.getIterator();

		// location and range has to be scaled
		X = (int) (X / (GlobalZoom / 100));
		Y = (int) (Y / (GlobalZoom / 100));

		// turning this off helps with testing as you can then see the panels
		// appearing/vanishing when zoomed out
		Range = (int) (Range / (GlobalZoom / 100));
		//

		Log.warning("____Removing panels outside: " + Range + " each way of "
				+ X);
		Log.info("___log2");
		while (panels.hasNext()) {
			PanelInfo paneltotest = panels.next();

			ComicPanel panel = AllComicPanels.GetByID(paneltotest.ID);

			if (panel == null) {
				Log.info("panel " + paneltotest.ID + " doesnt exist");
				continue;
			}

			// make sure panel is attached.
			if (!(AllLoadedComicPanelsInfo.IDexists(paneltotest.ID))) {
				// if it isn't we just skip it
				continue;
			}

			if ((paneltotest.LocationX > (X + Range))
					|| (((paneltotest.LocationX + paneltotest.SizeX) < (X - Range)))) {

				RemoveList.add(panel);

				continue;
			}

			if ((paneltotest.LocationY > (Y + Range))
					|| (((paneltotest.LocationY + paneltotest.SizeY) < (Y - Range)))) {
				RemoveList.add(panel);

				continue;
			}

		}
		// remove all
		removeThesePanels(RemoveList);
	}

	static void removeThesePanels(ArrayList<ComicPanel> RemoveList) {
		Iterator<ComicPanel> RemoveThese = RemoveList.iterator();
		while (RemoveThese.hasNext()) {

			ComicPanel removethis = RemoveThese.next();
			Log.info("removing:" + removethis.IDNumber);

			// remove from comic panel layer?
			int layernum = AllLoadedComicPanelsInfo
					.GetByID(removethis.IDNumber).onLayerNumber;
			Log.info("removing " + removethis.IDNumber + " from layer"
					+ layernum);
			layers.get(layernum).removePanel(removethis);
			// ------

			// AbsolutePanel Parent = ((AbsolutePanel) removethis.getParent());
			// Parent.remove(removethis);

			ContainerPanel.remove(removethis);

			// dont remove from global store!
			// AllComicPanels.remove(removethis);
			AllLoadedComicPanelsInfo.RemoveByID(removethis.IDNumber);

		}
	}

	static void setEditMode(boolean editOn){
		
		if (panelstreamer.EDITMODE == true){
			
			if (panelstreamer.PanelBeingEdited != null){
				panelstreamer.PanelBeingEdited.setEditModeOn(false);
			}
			
			
		}			
				
		panelstreamer.EDITMODE = editOn;
		
		if (panelstreamer.EDITMODE ){
			
		//	RootPanel.get().add(infoPanel, Window.getClientWidth()-390 , MainControlBar.getOffsetHeight()-30);
			
			if (!infoPanel.rolloutbit.isOpen()){
			infoPanel.rolloutbit.setOpen(true);
			}

			BottomBar.setStatusLab("Edit Status:");
		} else {
			
			if (!infoPanel.rolloutbit.isOpen()){
				infoPanel.rolloutbit.setOpen(false);
			}

			BottomBar.setStatusLab("Status:");
		}
		
	}
	
	/** resize keeping the mouse centre **/
	static void ResizeAroundMouse(double Percent, int X, int Y) {

		// int relMouseX = X - (ContainerPanel.ContainerSizeX/2);
		// int relMouseY = Y - (ContainerPanel.ContainerSizeY/2);

		// convert to absolute position
		// X = X - ContainerPanel.left;
		// Y = Y - ContainerPanel.top;

		// Window.alert("move center to position");
		// currentXpos = X;
		// currentYpos = Y;
		// ContainerPanel.setPosition(X, Y);

		// Window.alert("resize");
		
		//Resizer(Percent);
		
		changeGlobalZoom(Percent);
		
		/*
		 * Window.setTitle(currentXpos+":move back:"+relMouseX);
		 * ContainerPanel.setPosition(currentXpos -relMouseX , currentYpos -
		 * relMouseY); currentXpos = currentXpos -relMouseX; currentYpos =
		 * currentYpos - relMouseY;
		 */

		// Distance to the center of the screen.
		// int DisX = X - (ContainerSizeX/2);
		// int DisY = Y - (ContainerSizeY/2);

		// Scale
		// int DisXscale = (int) (DisX/(GlobalZoom/100));
		// int DisYscale = (int) (DisY/(GlobalZoom/100));

		// currentXpos = currentXpos - DisXscale;
		// currentYpos = currentYpos - DisYscale;
		/*
		 * Resizer(Percent);
		 * 
		 * //convert the mouse position to the new scale int NewWX = (int) (X *
		 * (Percent/100)); int NewWY = (int) (Y * (Percent/100));
		 * 
		 * 
		 * 
		 * currentXpos =NewX; currentYpos =NewY;
		 * 
		 * 
		 * 
		 * ContainerPanel.setPosition(currentXpos, currentYpos);
		 */
		/*
		 * //displacement of mouse from center. int DisX = X -
		 * (ContainerSizeX/2); int DisY = Y - (ContainerSizeY/2);
		 * 
		 * //displacement nesscery to keep mouse point where it is int DisXscale
		 * = (int)(DisX - ((GlobalZoom/100)*DisX)); int DisYscale = (int)(DisY -
		 * ((GlobalZoom/100)*DisY));
		 * 
		 * 
		 * currentXpos = currentXpos + DisXscale; currentYpos = currentYpos +
		 * DisYscale;
		 * 
		 * lab_currentDisX.setText("-"+DisX+"=");
		 * lab_currentDisY.setText("-"+DisY+"=");
		 */

	}

	/** safely change the global zoom **/
	static void changeGlobalZoom(double Percent) {

		// if near a 100 round up/down to it
		if ((panelstreamer.GlobalZoom > 95) && (panelstreamer.GlobalZoom < 105)) {
			
			Log.info("______rounding too:"+panelstreamer.GlobalZoom );
			
			panelstreamer.GlobalZoom = 100;
		}

		panelstreamer.Resizer(panelstreamer.GlobalZoom);

	}

	static void Resizer(double Percent) {

		// prevent zoom more intense then 400
		Window.setTitle("global zoom=" + GlobalZoom);
		// if (GlobalZoom>250){
		// Window.setTitle("its over the limit=");
		// GlobalZoom=250;
		// ChangeInZoom=100;
		// return;
		// }

		// work out what quality mode we should be;
		String oldQuality = currentQuality;

		currentQuality = HIGH_QUALITY_LOCATION;

		if (GlobalZoom < 50) {
			currentQuality = MEDIUM_QUALITY_LOCATION;
		}
		if (GlobalZoom < 25) {
			currentQuality = LOW_QUALITY_LOCATION;
		}

		// reset the size
		Log.info("______________________setting size to -10000");
		ContainerPanel.setMaxSize(-10000, -10000);

		// we loop over resizing each layer...
		// loop for all layers in file
		int CurrentLayerNum = 0;

		// Window.setTitle("starting loops");
		// while (CurrentLayerNum < layers.size()) {
		// Window.setTitle("resizeing layer:"+CurrentLayerNum);

		// AbsolutePanel CurrentLayer = layers.get(CurrentLayerNum);
		// ComicPanelLayer CurrentLayer = layers.get(CurrentLayerNum);

		// each panel on the layer
		// int total = CurrentLayer.getWidgetCount();
		// int total = CurrentLayer.comicPanelsOnLayer.size();
		// int tw = ContainerPanel.dragableLayer.getWidgetCount();
		// Log.info("total size="+total+" total widgets = "+tw);

		Iterator<ComicPanel> panelIT = AllComicPanels.GetPanelIterator();

		int current = 0;
		// while (current < total) {

		while (panelIT.hasNext()) {

			// Window.setTitle("Resizing panel:"+current);
			// Log.info("resizing panel=" + current + " total widgets = "
			// + total);

			// int PanelsID =
			// ((ComicPanel)CurrentLayer.getWidget(current)).IDNumber;
			// int PanelsID = ((ComicPanel) CurrentLayer.comicPanelsOnLayer
			// .get(current)).IDNumber;

			ComicPanel setThisPanel = panelIT.next();

			int PanelsID = setThisPanel.IDNumber;
			int originalX = 0;
			int originalY = 0;
			int originalWidth = 0;
			int originalHeight = 0;

			Log.info("___-_______Resizing panel id:" + PanelsID
					+ "       <--------");

			// Window.setTitle("detect if helper:"+current);
			// if its the loading helper we use its own values
			if (PanelsID == 9999) {
				originalX = 0;
				originalY = 0;
				originalWidth = 2000;
				originalHeight = 2000;
			} else {
				// Window.setTitle("non helper:");
				// Get original values

				originalX = AllComicPanelsInfo.GetByID(PanelsID).LocationX;
				originalY = AllComicPanelsInfo.GetByID(PanelsID).LocationY;
				originalWidth = AllComicPanelsInfo.GetByID(PanelsID).SizeX;
				originalHeight = AllComicPanelsInfo.GetByID(PanelsID).SizeY;
			}

			// special case for 100% zoom (ie, normal size)
			if (Percent == 100) {
				// set size
				setThisPanel.setPixelSize(originalWidth, originalHeight);

				// set position
				if (setThisPanel.isAttached()) {
					ContainerPanel
							.movePanel(setThisPanel, originalX, originalY);

				} else {
					Log.info("comic panel ID:" + setThisPanel.IDNumber
							+ " Type " + setThisPanel.PanelType
							+ " is not attached");

				}
			} else {

				int newX = (int) Math.round(originalX * (Percent / 100));
				int newY = (int) Math.round(originalY * (Percent / 100));
				// Window.setTitle("setting position :"+newX+" "+newY);

				Log.info("setting location");

				// CurrentLayer.setWidgetPosition(CurrentLayer.getWidget(current),
				// newX, newY);

				// ComicPanel setThisPanel = CurrentLayer.comicPanelsOnLayer
				// .get(current);

				// set size
				setThisPanel.setPixelSize(
						(int) (originalWidth * (Percent / 100)),
						(int) (originalHeight * (Percent / 100)));

				// set position
				if (setThisPanel.isAttached()) {

					ContainerPanel.movePanel(setThisPanel, newX, newY);

					// ContainerPanel.dragableLayer.setWidgetPosition(
					// setThisPanel, newX, newY);

				} else {
					Log.info("comic panel ID:" + setThisPanel.IDNumber
							+ " Type " + setThisPanel.PanelType
							+ " is not attached");

				}
			}
			// ((ComicPanel)CurrentLayer.getWidget(current)).setPixelSize(
			// (int)(originalWidth * (Percent/100)), (int)(originalHeight *
			// (Percent/100)));
			// Window.setTitle("set size of "+current+" to :"+(int)(originalWidth
			// * (Percent/100))+" from "+originalWidth);
			// also update the url to reflect quality

			// if (((ComicPanel)CurrentLayer.getWidget(current)).PanalType
			// == "image") {

			if (setThisPanel.PanelType == ComicPanel.PanelTypes.IMAGE) {

				// ComicPanel currentpanel = ((ComicPanel)
				// CurrentLayer.comicPanelsOnLayer
				// .get(current));
				// Image panelsImage = ((Image) ((ComicPanel)
				// CurrentLayer.comicPanelsOnLayer
				// .get(current)).contents);
				ComicPanel currentpanel = setThisPanel;
				Image panelsImage = (Image) setThisPanel.contents;

				panelsImage.setUrl(panelsImage.getUrl().replaceAll(oldQuality,
						currentQuality));

				// reset the zdepth, as this can go wrong
				// ((ComicPanel)CurrentLayer.getWidget(current)).getElement().getStyle().setProperty("zIndex",
				// ""+currentpanal.originalZDepth);

				// ((ComicPanel) CurrentLayer.comicPanelsOnLayer.get(current))
				// .getElement().getStyle().setProperty("zIndex",
				// "" + currentpanel.panelsZDepth);

				setThisPanel.getElement().getStyle().setProperty("zIndex",
						"" + currentpanel.panelsZDepth);
			}

			// ((ComicPanel)CurrentLayer.getWidget(current)).setHeight(originalHeight
			// * (Percent/100)+"px");

			// System.out.print("\n"+OldHeight+"-"+OldWidth+">"+OldHeight *
			// (Percent/100)+"px"+"-"+OldWidth * (Percent/100)+"px");
			// Window.setTitle("resized panel:"+current);

			current++;
		}
		// Window.setTitle("resized layer:"+CurrentLayerNum);

		CurrentLayerNum++;

		// }

		// Finally reposition the view around the center
		Log.info("______________________setting position");

		
		// Doesnt deal with 100% zoom correctly;
		
		// check if new total size is less then containers html size
		if (ContainerPanel.getMaxHeight() <= ContainerPanel.ContainerSizeY) {
			Log.info("\n \n \n___(centeringh)" + ContainerPanel.getMaxHeight());
			currentYpos = (int) Math.round(ContainerPanel.getMaxHeight() / 2.0);
			ContainerPanel.YMOVEMENTDISABLED = true;

		} else {
			currentYpos = (int) Math.round(currentYpos * (ChangeInZoom / 100));
			ContainerPanel.YMOVEMENTDISABLED = false;

		}

		Log.info("\n Old X = " + currentXpos);

		if (ContainerPanel.getMaxWidth() <= ContainerPanel.ContainerSizeX) {
			Log.info("\n \n \n \n ___(centering w)"
					+ ContainerPanel.getMaxWidth());
			currentXpos = (int) Math.round(ContainerPanel.getMaxWidth() / 2.0);
			ContainerPanel.XMOVEMENTDISABLED = true;
		} else {
			currentXpos = (int) Math.round(currentXpos * (ChangeInZoom / 100));
			ContainerPanel.XMOVEMENTDISABLED = false;
		}
		Log.info("\n New X = " + currentXpos);

		ContainerPanel.setPosition(currentXpos, currentYpos);

		MainControlBar.setCurrentPositionLabel(currentXpos, currentYpos);

		// update the helper widget
		ContainerPanel.dragableLayer.setWidgetPosition(LoadingHelper,
				currentXpos - (int) Math.round(1000 * (Percent / 100)),
				currentYpos - (int) Math.round(1000 * (Percent / 100)));

		// update panels position
		addPanelsWithin(currentXpos, currentYpos,
				(int) (1000 * (100 / GlobalZoom)));

	}

	public void onValueChange(ValueChangeEvent<String> event) {

		// history changed
		String historyToken = event.getValue();

		updateBasedOnHistoryToken(historyToken);

	}

}
