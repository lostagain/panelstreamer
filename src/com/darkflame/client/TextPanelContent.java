package com.darkflame.client;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SimpleHtmlSanitizer;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

public class TextPanelContent extends VerticalPanel {
	HTML text;
	
	public TextPanelContent(HTML TextC){
	
		text =TextC;
		
		text.setHTML(text.getHTML().replaceAll("\\\\n", "<br>"));
		
		
		this.setStylePrimaryName("TextPanel");
	
	this.add(text);
	
	this.setCellVerticalAlignment(text,
			VerticalPanel.ALIGN_MIDDLE);
	
	

	}
	
	public String getText(){
		return text.getHTML();
	}
	public  void setHTML(String newtext){
		
		//sanity testing here (useless - it escapes newlines :-/)
		//SafeHtml safeHTML = SimpleHtmlSanitizer.sanitizeHtml(newtext);
		
		text.setHTML(newtext);
		
		return ;
		
	}
	public  void setText(String newtext){
		text.setText(newtext);
		return ;
		
	}
}
