package com.darkflame.client;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

public class BottomBar  extends HorizontalPanel {
	
	public static Label mouseStatus = new Label("---");
	public static Label numofFingersDown = new Label("---");

	public static Label statusLab = new Label("Status:");
	
	public static void setStatusLab(String statusLab) {
		BottomBar.statusLab.setText(statusLab);
	}

	public BottomBar(){
		
		super.setVerticalAlignment(ALIGN_MIDDLE);
		
		mouseStatus.addStyleName("unselectable");
		numofFingersDown.addStyleName("unselectable");
		statusLab.addStyleName("unselectable");
		
		super.add(statusLab);
		
		super.add(mouseStatus);
		super.add(numofFingersDown);
		
		super.setWidth("500px");
		super.setHeight("50px");
		
		this.getElement().getStyle().setColor("WHITE");
		this.getElement().getStyle().setZIndex(900000);
		
		this.setStylePrimaryName("toolbar");
		this.addStyleName("unselectable");
		
		numofFingersDown.setStylePrimaryName("mouseStatusLabel");
		mouseStatus.setStylePrimaryName("mouseStatusLabel");
		statusLab.setStylePrimaryName("mouseStatusLabel");
		
	}
	
	static public void setNumofFingersDown(int i){
		numofFingersDown.setText("--"+i);
	}
	
	static public void setMouseStatus(String lab){
		mouseStatus.setText(lab);
	}

}
