package com.darkflame.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;


public class GlobalPanelInfoStore {

//	final ArrayList <PanelInfo> AllPanels = new ArrayList<PanelInfo>();
	final HashMap <Integer,PanelInfo> AllPanels = new HashMap <Integer,PanelInfo>();
	static Logger Log = Logger.getLogger("GlobalPanelInfoStoreLog");
	 
	public GlobalPanelInfoStore(){
		
	}
	public void clear() {
		AllPanels.clear();
	}
		
	public void add(PanelInfo newcomicpanelingo){
		AllPanels.put(newcomicpanelingo.ID,newcomicpanelingo);
	}
	
	public void remove(PanelInfo comicpanelinfo){
		AllPanels.remove(comicpanelinfo.ID);
	}

	public PanelInfo GetByID(int IDrequested){
		
		Log.info("looking for id:"+IDrequested);
		
		PanelInfo ThisPanel = AllPanels.get(IDrequested);

		Log.info("found id:"+ThisPanel.ID);
		
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				ThisPanel = CurrentPanel;
			}
			
		}
		*/
		
		return ThisPanel;
	}
public void RemoveByID(int IDrequested){
		
		PanelInfo ThisPanel = AllPanels.get(IDrequested);
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				ThisPanel = CurrentPanel;
			}
			
		}
		*/
		
		AllPanels.remove(ThisPanel);
	}
	
	public Iterator<PanelInfo> getIterator(){		
		return AllPanels.values().iterator();
	}
	public boolean IDexists(int IDrequested){
		
		boolean state = AllPanels.containsKey(IDrequested);
		Log.info("id exists:"+state);
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				state = true;
			}
			
		}*/
		
		
		return state;
	}
}
