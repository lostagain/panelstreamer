package com.darkflame.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

/** A clickable title that triggers the dropdown filelist **/

public class LayerListPopup extends Label {

	static String title = "( Layers )";
	PopupPanel layerlistPopup;
	Label titlelab = this;
	
	boolean popupShowing=false;
	
	public LayerListPopup(final layerList popupContents){
		
		super(title);
		//this.setStylePrimaryName("TitleLabel");
		this.setHeight("100%");
		this.setText(title);
		
		this.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				if (popupShowing==false){
				// open the filelist popup
					layerlistPopup = new PopupPanel();
					layerlistPopup.setAutoHideEnabled(true);
					
					layerlistPopup.add(popupContents);
					layerlistPopup.setPopupPosition(titlelab.getAbsoluteLeft()+15, titlelab.getAbsoluteTop()+29);
					
					layerlistPopup.getElement().getStyle().setZIndex(900001);
					layerlistPopup.show();
				    popupShowing=true;
				    
				} else {
					layerlistPopup.hide();
					popupShowing=false;
				}
			}
			
		});
		
	}
	
	public void setTitle(String newTitle){
		title=newTitle;
		this.setText(title);
	
	}
	
	public void closePopUp()
	{
		if (popupShowing){
		layerlistPopup.hide();	
		popupShowing=false;
		}
	}
	
}
