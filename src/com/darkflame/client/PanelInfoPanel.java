package com.darkflame.client;

import java.awt.Color;
import java.util.Iterator;
import java.util.logging.Logger;

import com.darkflame.client.ComicPanel.PanelTypes;
import com.darkflame.client.PanelInfo.PositionInPanel;
import com.darkflame.client.RichTextToolbar.RichTextToolbar;
import com.darkflame.client.RichTextToolbar.RichTextToolbar.PostClickAction;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DisclosurePanelImages;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox.DefaultSuggestionDisplay;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.NumberLabel;

public class PanelInfoPanel extends Composite {

	interface MyStyles extends CssResource {
		String important();

		String blueBack();
	}

	static Logger Log = Logger.getLogger("InfoPanelPanel");
	private MultiWordSuggestOracle urlSuggestList = new MultiWordSuggestOracle();
	private MultiWordSuggestOracle PIDlist = new MultiWordSuggestOracle();
	private MultiWordSuggestOracle ColourSuggestions = new MultiWordSuggestOracle();
	@UiField
	IntegerBox Size_X;
	@UiField
	IntegerBox Size_Y;
	@UiField
	IntegerBox Loc_X;
	@UiField
	IntegerBox Loc_Y;
	@UiField
	IntegerBox Loc_Z;

	@UiField
	MyStyles style;
	@UiField
	Button UpdatePanel;
	@UiField
	ListBox panelTypeList;
	@UiField
	ListBox ActionTypes;
	@UiField
	ListBox threadFromLocDropdown;
	@UiField(provided = true)
	SuggestBox threadfromID;
	@UiField
	ListBox threadToLocDropdown;
	@UiField(provided = true)
	SuggestBox threadtoID;
	
	@UiField(provided = true) SuggestBox threadColourList;

	@UiField
	DisclosurePanel threadSettings;
	@UiField
	ToggleButton RatioToggle;
	@UiField
	TextBox ActionSetting;
	@UiField
	RichTextArea Text_Data;
	@UiField
	SimplePanel TextControlContainer;
	@UiField
	Label PanelTitle;
	@UiField
	ListBox LayerList;
	@UiField
	HorizontalPanel c;
	
	@UiField(provided = true) SuggestBox URL_Data;

	@UiField
	PushButton setOriginalSize;
	@UiField
	CheckBox autoupdateCheckbox;
	@UiField DisclosurePanel actionSettings;
	@UiField NumberLabel<Integer> oriImageSizeX;
	@UiField NumberLabel<Integer> oriImageSizeY;
	
	@UiField Image oriImage;
	@UiField DisclosurePanel imageDetails;

	DisclosurePanel rolloutbit = new DisclosurePanel("Edit Mode");
	//dectoration panel
	DecoratorPanel decContainer = new DecoratorPanel();
	
	final Label HeaderLabel = new Label("Edit Mode");
	// rtb controls
	RichTextToolbar TextControls;

	final DisclosurePanelImages dpimages = (DisclosurePanelImages) GWT
			.create(DisclosurePanelImages.class);

	DisclosurePanelHeader DisclosureHeader = new DisclosurePanelHeader();

	private static PanelInfoPanelUiBinder uiBinder = GWT
			.create(PanelInfoPanelUiBinder.class);

	// current ID being edited
	static int currentPID = 0;
	// current type being edited
	static ComicPanel.PanelTypes currentType = null;
	// Maintain size ratio on/off
	Boolean keepRatio = false;
	Double ratio = 1.0;
	private Boolean autoUpdateOn = true;

	interface PanelInfoPanelUiBinder extends UiBinder<Widget, PanelInfoPanel> {
	}

	// SuggestBox URL_Data;

	public PanelInfoPanel() {

		URL_Data = new SuggestBox(urlSuggestList);
		threadfromID = new SuggestBox(PIDlist);
		threadtoID = new SuggestBox(PIDlist);
		threadColourList = new SuggestBox(ColourSuggestions);
		
		URL_Data.addChangeListener(new ChangeListener() {

			@Override
			public void onChange(Widget sender) {

				Log.info("url changed 2");
				updateData();
			}

		});

		URL_Data.getTextBox().addBlurHandler(new BlurHandler() {

			@Override
			public void onBlur(BlurEvent event) {

				Log.info("url changed");
				updateData();
			}

		});
		
		threadColourList.getTextBox().addBlurHandler(new BlurHandler() {

			@Override
			public void onBlur(BlurEvent event) {

				Log.info("threadColourList changed");
				
				if (threadColourList.getText().contains(",") && (!threadColourList.getText().startsWith("rgb")) ){
					//assume they are trying to enter a RGB value
					threadColourList.setText("rgb("+threadColourList.getText()+")");
							
				}
				
			
				if (autoUpdateOn){
					
				updateData();
				
				}
			}

		});
		//decContainer.add(rolloutbit);
		initWidget(rolloutbit);
		rolloutbit.setContent(uiBinder.createAndBindUi(this));
		
		//set animation satus
		rolloutbit.setAnimationEnabled(true);
		threadSettings.setAnimationEnabled(true);
		actionSettings.setAnimationEnabled(true);
		imageDetails.setAnimationEnabled(true);
		
		this.getElement().getStyle().setZIndex(900001);

		setURLSuggestions();
		// URL_Data_container.setWidget(URL_Data);

		// URL_Data.setLimit(5);
		// URL_Data.setAutoSelectEnabled(true);
		// URL_Data.showSuggestionList();

		// text controls
		// text editing controls
		TextControls = new RichTextToolbar(Text_Data);
		TextControlContainer.add(TextControls);
		TextControls.setVisible(false);
		TextControls.setPostAction(new PostClickAction() {
			@Override
			public void RTFInteractionTriggered() {
				updateData();
			}

		});
		//

		rolloutbit.setHeader(DisclosureHeader);
		rolloutbit.getHeader().setWidth(430 + "px");

		HeaderLabel.setStyleName(style.important());

		// rolloutbit.getHeader().getElement().getStyle().setColor("FFFFFF");
		// rolloutbit.getHeader().getParent().getElement().getStyle().setColor("White");

		rolloutbit.addOpenHandler(new OpenHandler<DisclosurePanel>() {
			@Override
			public void onOpen(OpenEvent<DisclosurePanel> event) {
				panelstreamer.setEditMode(true);
				DisclosureHeader.setOpen();
			}
		});
		rolloutbit.addCloseHandler(new CloseHandler<DisclosurePanel>() {
			@Override
			public void onClose(CloseEvent<DisclosurePanel> event) {
				panelstreamer.setEditMode(false);
				DisclosureHeader.setClosed();
			}
		});

		// rolloutbit.getElement().addClassName(style.blueBack());
		// rolloutbit.getElement().getStyle().setZIndex(999999);

		// rolloutbit.setAnimationEnabled(true);

		// this.getElement().getStyle().setZIndex(999999);

		// this.getElement().getStyle().setColor("FFFFFF");
		// rolloutbit.setStyleName("whitetext");
		// this.getElement().getStyle().setBackgroundColor("000000");

		setUpTypeList();
		setUpActionTypeList();
		setColourList();

		setThreadLocList(threadFromLocDropdown);
		setThreadLocList(threadToLocDropdown);
		setIDLists();
		setUpLayerList();
	}

	class DisclosurePanelHeader extends HorizontalPanel {
		AbstractImagePrototype closedImage = dpimages.disclosurePanelClosed();
		AbstractImagePrototype openImage = dpimages.disclosurePanelOpen();
		Image arrowImage = closedImage.createImage();
		final Label SPACER = new Label(" ");

		public DisclosurePanelHeader() {

			setWidth("370px");
			setHorizontalAlignment(ALIGN_RIGHT);
			add(HeaderLabel);
			add(arrowImage);
			setCellWidth(arrowImage, "18px");
			add(SPACER);
			setCellWidth(SPACER, "60px");

		}

		public void setOpen() {
			openImage.applyTo(arrowImage);
		}

		public void setClosed() {
			closedImage.applyTo(arrowImage);
		}
	}

	private void setUpLayerList() {

		Iterator<layerInfo> lit = panelstreamer.layersinfo.iterator();
		int layernum = 0;
		while (lit.hasNext()) {

			layerInfo cl = lit.next();

			LayerList.addItem(cl.Title + " (" + layernum + ")");
			layernum++;
		}

	}

	private void setUpTypeList() {
		panelTypeList.addItem("Image");
		panelTypeList.addItem("TextBox");
		panelTypeList.addItem("Thread");
		panelTypeList.addItem("WebPage");
		panelTypeList.setEnabled(false);

	}

	private void setUpActionTypeList() {
		ActionTypes.addItem("None");
		ActionTypes.addItem("Link To");
		
		ActionTypes.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				
				if (ActionTypes.getSelectedIndex() != 0){
					ActionSetting.setEnabled(true);
				} else {
					ActionSetting.setEnabled(false);
				}
				
			}
		});

	}

	private void setColourList() {
		
		ColourSuggestions.add("White");
		ColourSuggestions.add("Black");	

		ColourSuggestions.add("Blue");
		ColourSuggestions.add("Green");
		ColourSuggestions.add("Yellow");
		ColourSuggestions.add("Orange");
		ColourSuggestions.add("Red");
		ColourSuggestions.add("Fuchsia"); 

	}

	private void setThreadLocList(ListBox box) {
		
		// Loop over PanelInfo.PositionInPanel.values() and add them all
		int numOfValues = PanelInfo.PositionInPanel.values().length;
		int i=0;
		
		while (i<numOfValues){
			
			PanelInfo.PositionInPanel positionType = PanelInfo.PositionInPanel.values()[i];
			box.addItem(positionType.toString());
			
			i++;
		}
		

	}

	private void setIDLists() {

		Iterator<Integer> it = CollectionStats.AllIDs.iterator();
		Log.info("num of known ids = " + CollectionStats.AllIDs.size());

		while (it.hasNext()) {
			Integer cid = it.next();
			PIDlist.add("" + cid);

		}

	}

	public void refreshBoxs() {

		setIDLists();
		setURLSuggestions();
		setUpLayerList();
	}

	private void setURLSuggestions() {

		urlSuggestList.clear();

		if (currentType == ComicPanel.PanelTypes.WEBPAGE) {

			Log.info("________________________________adding webpage suggestions- ");

			Iterator<String> iURL = CollectionStats.WebpageURLs.iterator();

			while (iURL.hasNext()) {

				String string = (String) iURL.next();
				urlSuggestList.add(string);

			}
		} else {
			// (currentType == ComicPanel.PanelTypes.TEXTBOX)
			Log.info("________________________________adding image suggestions- ");
			Iterator<String> iURL = CollectionStats.ImageURLs.iterator();

			while (iURL.hasNext()) {

				String string = (String) iURL.next();
				// Log.info("________________________________adding image:"+string);

				// add string with/without full url

				urlSuggestList.add(string);
				if (!string.startsWith("http")) {
					urlSuggestList.add(GWT.getHostPageBaseURL() + string);
					// Log.info("________________________________adding image :"+GWT.getHostPageBaseURL()+string);
				}
			}

		}

		Log.info("________________________________added url suggestions- ");

		// get all the urls from main array

		// add them
	}

	
	public void clearData(){
		
		//location and size data shouldn't be cleared as everything should have that data

		
		//image clear
		oriImage.setVisible(false);
		oriImageSizeX.setValue(0);
		oriImageSizeY.setValue(0);
		//text clear
		Text_Data.setHTML("");
		Text_Data.setEnabled(false);
		TextControls.setVisible(false);
		//url clear
		URL_Data.setText("");
		URL_Data.getTextBox().setEnabled(false);
		//type clear
		currentType =  null;
		//id clear
		currentPID = -1;
		
		//thread clear		
		threadfromID.setText("");
		threadtoID.setText("");
		threadColourList.setText("");
		
		///action reset
		ActionTypes.setSelectedIndex(0);
		ActionSetting.setValue("");
		ActionSetting.setEnabled(false);
	}
	
	public void setData(PanelInfo data) {
		
		//clear data
		clearData();
		
		//close if not thread
		if (data.Type!=ComicPanel.PanelTypes.THREAD){
		threadSettings.setOpen(false);
		}
		
		// set title
		PanelTitle.setText("Current Selected Panel; " + data.ID);

		if (data.Type == ComicPanel.PanelTypes.IMAGE) {

			panelTypeList.setSelectedIndex(0);
			URL_Data.setText(data.urlData);
			//Text_Data.setHTML("");
		//	Text_Data.setEnabled(false);
		//	threadSettings.setOpen(false);
		//	TextControls.setVisible(false);
			URL_Data.getTextBox().setEnabled(true);
			
			oriImage.setUrl(data.urlData);
			oriImage.setVisible(true);
			XYPoint imageSizeData = CollectionStats.ImageData.get(data.urlData);
			
			//fix image for smaller sizes then the container
			if ((imageSizeData.x<this.getOffsetWidth())&&(imageSizeData.y<400)){
				
				oriImage.setWidth(imageSizeData.x+"px");
				
			}
			
			oriImageSizeX.setValue(imageSizeData.x);
			oriImageSizeY.setValue(imageSizeData.y);
			
			
			
		}
		if (data.Type == ComicPanel.PanelTypes.TEXTBOX) {
			panelTypeList.setSelectedIndex(1);
			// set up text box data
			Text_Data.setEnabled(true);
			Text_Data.setHTML(data.TextString);
		//	URL_Data.setText("");
			TextControls.setVisible(true);
			//URL_Data.getTextBox().setEnabled(false);
			//threadSettings.setOpen(false);

		}
		if (data.Type == ComicPanel.PanelTypes.THREAD) {
			panelTypeList.setSelectedIndex(2);
			// set up thread data
			if (!threadSettings.isOpen()){
			threadSettings.setOpen(true);
			}
			threadfromID.setText(""+data.DrawFrom.PanelID);		
			threadFromLocDropdown.setSelectedIndex(data.DrawFrom.PositionInPanel.ordinal());
			
			threadToLocDropdown.setSelectedIndex(data.DrawTo.PositionInPanel.ordinal());
			threadtoID.setText(""+data.DrawTo.PanelID);
			
			//set colour
			threadColourList.setText(data.DrawColour.value());
			
			Log.info("___THREAD LOCATION X="+data.LocationX);
			
			
		//	TextControls.setVisible(false);
			
			//URL_Data.getTextBox().setEnabled(false);
			// Thread_Data.setText(data.Data);
		}

		if (data.Type == ComicPanel.PanelTypes.WEBPAGE) {
			panelTypeList.setSelectedIndex(3);

			URL_Data.setText(data.urlData);

			URL_Data.getTextBox().setEnabled(true);
		//	TextControls.setVisible(false);
		//	Text_Data.setText("");
		//	Text_Data.setEnabled(false);
		//	threadSettings.setOpen(false);
		}

		// if it has actions
		if (data.LinkTo != null) {
			ActionTypes.setSelectedIndex(1);
			ActionSetting.setValue(data.LinkTo);
			ActionSetting.setEnabled(true);
		} else {
			//ActionTypes.setSelectedIndex(0);
			//ActionSetting.setValue("");
			//ActionSetting.setEnabled(false);
		}
		currentPID = data.ID;

		currentType = data.Type;

		Size_X.setValue(data.SizeX);
		Size_Y.setValue(data.SizeY);
		ratio = (double) (Size_X.getValue() / Size_Y.getValue());

		setLocationData(data.LocationX,data.LocationY,data.LocationZ);

		LayerList.setSelectedIndex(data.onLayerNumber);

		setURLSuggestions();
	}

	public void setLocationData(int locationX, int locationY, int locationZ) {
		
		Loc_X.setValue(locationX);
		Loc_Y.setValue(locationY);
		Loc_Z.setValue(locationZ);
		
	}

	/** updates the panel to reflect changes to this box **/
	private void updateData() {

		int x = Loc_X.getValue();
		int y = Loc_Y.getValue();
		int z = Loc_Z.getValue();
		int sizeX = Size_X.getValue();
		int sizeY = Size_Y.getValue();

		String Text = Text_Data.getHTML();
		String URL = URL_Data.getText();
		String linkTo = ActionSetting.getText();
		int LayerNum = LayerList.getSelectedIndex();

		Log.info("new size x = " + sizeX);

		PanelInfo newData = new PanelInfo(currentPID, currentType, URL, Text,
				x, y, z, sizeX, sizeY, linkTo, LayerNum);
		
		if (currentType == PanelTypes.THREAD){
			
			//set up thread data
			newData.DrawColour = CssColor.make(threadColourList.getValue());
			newData.DrawFrom = null;
			newData.DrawTo = null;
			
		}
		
		panelstreamer.PanelBeingEdited.updateData(newData);

	}

	@UiHandler("UpdatePanel")
	void handleClick(ClickEvent e) {
		updateData();
	}

	@UiHandler("Size_X")
	void onSize_XBlur(BlurEvent event) {
		if (autoUpdateOn) {
			if (keepRatio) {
				Size_Y.setValue((int) (Size_X.getValue() / ratio));

			}
			updateData();
		}
	}

	@UiHandler("Size_X")
	void onSize_XKeyPress(KeyPressEvent event) {
		if (autoUpdateOn) {
			if (event.getCharCode() == 13) {
				if (keepRatio) {
					Size_Y.setValue((int) (Size_X.getValue() / ratio));

				}
				updateData();
			}
		}
	}

	@UiHandler("Size_Y")
	void onSize_YBlur(BlurEvent event) {
		if (autoUpdateOn) {
			if (keepRatio) {
				Size_X.setValue((int) (Size_Y.getValue() * ratio));
			}

			updateData();
		}
	}

	@UiHandler("Size_Y")
	void onSize_YKeyPress(KeyPressEvent event) {
		if (autoUpdateOn) {
			if (event.getCharCode() == 13) {
				if (keepRatio) {
					Size_X.setValue((int) (Size_Y.getValue() * ratio));

				}
				updateData();
			}
		}
	}

	// @UiHandler("Text_Data")
	// void onText_DataChange(ChangeEvent event) {
	// updateData();
	// }

	@UiHandler("Loc_Z")
	void onLoc_ZBlur(BlurEvent event) {
		if (autoUpdateOn) {
			updateData();
		}
	}

	@UiHandler("Loc_X")
	void onLoc_XBlur(BlurEvent event) {
		if (autoUpdateOn) {
			updateData();
		}
	}

	@UiHandler("Loc_Y")
	void onLoc_YKeyPress(KeyPressEvent event) {
		if (autoUpdateOn) {
			if (event.getCharCode() == 13) {
				updateData();
			}
		}
	}

	@UiHandler("Loc_X")
	void onLoc_XKeyPress(KeyPressEvent event) {
		if (autoUpdateOn) {
			if (event.getCharCode() == 13) {
				updateData();
			}
		}
	}

	@UiHandler("Loc_Z")
	void onLoc_ZKeyPress(KeyPressEvent event) {
		if (autoUpdateOn) {
			if (event.getCharCode() == 13) {
				updateData();
			}
		}
	}

	@UiHandler("Loc_Y")
	void onLoc_YBlur(BlurEvent event) {
		if (autoUpdateOn) {
			updateData();
		}
	}

	@UiHandler("Text_Data")
	void onText_Data_oldKeyUp(KeyUpEvent event) {
		if (autoUpdateOn) {
			updateData();
		}
	}

	@UiHandler("RatioToggle")
	void onRatioToggleClick(ClickEvent event) {

		if (RatioToggle.isDown()) {
			ratio = (double) (Size_X.getValue() / Size_Y.getValue());
			keepRatio = true;
		} else {
			keepRatio = false;
		}

	}

	@UiHandler("LayerList")
	void onLayerListChange(ChangeEvent event) {

		// update current layer
		if (autoUpdateOn) {
			updateData();
		}
	}

	@UiHandler("setOriginalSize")
	void onPushButtonClick(ClickEvent event) {

		// get the original image size
		XYPoint imageSizeData = CollectionStats.ImageData.get(URL_Data.getText());
		// set the size
		Size_X.setValue(imageSizeData.x);
		Size_Y.setValue(imageSizeData.y);
		// update current layer
				if (autoUpdateOn) {
					updateData();
				}
	}

	@UiHandler("autoupdateCheckbox")
	void onAutoupdateCheckboxClick(ClickEvent event) {

		autoUpdateOn = autoupdateCheckbox.getValue();

	}
}