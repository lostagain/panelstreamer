package com.darkflame.client.actions;

import java.util.logging.Logger;

import com.google.gwt.user.client.Window;

public class GotoURL implements ComicPanelAction {
    static Logger Log = Logger.getLogger("ComicPanelActionLog");

	TriggerTypes TriggerType = TriggerTypes.CLICK; //defaults to click
	String link ="";
	
	public GotoURL(String link){
		this.link=link;
	}
	
	@Override
	public void triggerAction() {
		 Log.info("(click detected)");
		 
		Window.open(link, "_blank", "");
	}

	@Override
	public TriggerTypes returnTriggerType() {
		// TODO Auto-generated method stub
		return TriggerType;
	}

}
