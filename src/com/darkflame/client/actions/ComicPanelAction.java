package com.darkflame.client.actions;

/** Interface class for all panel actions **/
public interface ComicPanelAction {

	 void triggerAction();
	
	 TriggerTypes TriggerType = TriggerTypes.CLICK;
	 
	 TriggerTypes returnTriggerType();
	
		static public enum TriggerTypes {

			CLICK, MOUSEOVER, DBLCLICK, ONVISIBLE, ONINVISIBLE, RIGHTMOUSECLICK

		}
	
}
