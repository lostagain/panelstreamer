package com.darkflame.client.actions;

import java.util.logging.Logger;

import com.darkflame.client.actions.ComicPanelAction.TriggerTypes;
import com.google.gwt.user.client.History;

public class GotoLocation implements ComicPanelAction {

	TriggerTypes TriggerType = TriggerTypes.CLICK; //defaults to click
	
	int x=0,y=0;
	String historyHash = "";

    static Logger Log = Logger.getLogger("ComicPanelActionLog");
    
	/** goes to the location/zoom specified by the hash tag on the url **/
	public GotoLocation(String historyHash){
		this.historyHash=historyHash;
	}
	
	public GotoLocation(int x,int y){
		this.x=x;
		this.y=y;
		
	}
	@Override
	public void triggerAction() {
		 Log.info("(click detected)");
		 History.newItem(historyHash,true);
			
		//moving the canvas to x/y
		//(insert code here!)
		
	}

	@Override
	public TriggerTypes returnTriggerType() {
		
		return TriggerType;
		
		// TODO Auto-generated method stub
		
	}


}
