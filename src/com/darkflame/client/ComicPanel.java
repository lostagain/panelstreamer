package com.darkflame.client;

import com.google.gwt.user.client.Event;

import java.util.Iterator;
import java.util.logging.Logger;

import com.darkflame.client.actions.ComicPanelAction;
import com.darkflame.client.actions.GotoLocation;
import com.darkflame.client.actions.GotoURL;
import com.darkflame.client.panelContentTypes.ThreadPanelContent;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.TouchCancelEvent;
import com.google.gwt.event.dom.client.TouchCancelHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * A single panel which can take any widget as its contents Typically, this
 * would be an image. It could also be a html snippet or a piece of text,
 * however.
 * **/

public class ComicPanel extends FocusPanel implements HasClickHandlers,
		ClickHandler, MouseDownHandler, MouseUpHandler, MouseMoveHandler,
		MouseOutHandler, ContextMenuHandler,
		TouchStartHandler,TouchMoveHandler,TouchEndHandler,TouchCancelHandler {

	public int IDNumber = 0;
	Widget contents;

	Logger Log = Logger.getLogger("ComicPanelLog");
	// String PanelType = "";
	PanelTypes PanelType;

	AbsolutePanel Frame = new AbsolutePanel();
	
	SpiffyBox Border;
	boolean BordersOn = false;
	

	int SizeX, SizeY = 0;
	int ScaledSizeX, ScaledSizeY = 0;
	
	int panelsZDepth;
	String linkUrl;

	Boolean EDITMODE = false;

	Boolean BEINGDRAGED = false;

	ComicPanelAction panelsAction = null;
	
	private static HandlerRegistration CHandel = null;
	private static HandlerRegistration MDHandeler = null;
	private static HandlerRegistration MUHandeler = null;
	private static HandlerRegistration MMHandeler = null;
	private static HandlerRegistration MOHandeler = null;
	private static HandlerRegistration COHandeler = null;
	//touch handlers
	private static HandlerRegistration TSHandeler = null;
	private static HandlerRegistration TMHandeler = null;
	private static HandlerRegistration TEHandeler = null;
	private static HandlerRegistration TCHandeler = null;
	//store the parent panel
	public AbsolutePanel ParentPanel = new AbsolutePanel();
	
	int dragStartX;
	int dragStartY;

	Boolean disableFocus=true;
	
	static public enum PanelTypes {

		IMAGE, 
		THREAD, 
		TEXTBOX, 
		WEBPAGE, 
		ECLIPSE,
		

	}

	/**
	 * create a new comic panel, which typical contains an image widget and the
	 * data needed to position it
	 **/
	public ComicPanel(Widget newcontents, PanelTypes setType, int IDNumberSet,
			int setSizeX, int setSizeY, int ZDepth, String LinkURL,
			boolean editable) {

		SetupComicPanel(newcontents, setType, IDNumberSet, setSizeX, setSizeY,
				ZDepth, LinkURL, editable);

		PanelType = setType;

	}

	/**
	 * create a new comicpanel, which typical contains an image widget and the
	 * data needed to position it
	 **/
	public ComicPanel(Widget newcontents, PanelTypes setType, int IDNumberSet,
			int setSizeX, int setSizeY, int ZDepth, String LinkURL) {

		SetupComicPanel(newcontents, setType, IDNumberSet, setSizeX, setSizeY,
				ZDepth, LinkURL, true);

	}

	public void SetupComicPanel(Widget newcontents, PanelTypes setType,
			int IDNumberSet, int setSizeX, int setSizeY, int ZDepth,
			String LinkURL, boolean editable) {
		linkUrl = LinkURL;

		SizeX = setSizeX; // original X
		SizeY = setSizeY; // original Y
		// work out scaled size
		ScaledSizeX = (int) (SizeX * (panelstreamer.GlobalZoom / 100));
		ScaledSizeY = (int) (SizeY * (panelstreamer.GlobalZoom / 100));

		panelsZDepth = ZDepth;

		this.getElement().getStyle().setZIndex(panelsZDepth);

		PanelType = setType;
		contents = newcontents;
		
		//make unselectable
		contents.addStyleName("unselectable");

		// make contents non-dragable
		contents.getElement().setAttribute("draggable", "false");
		contents.getElement().getStyle().setProperty("webkitUserDrag", "none");
		contents.getElement().setPropertyBoolean("draggable", false);

		// CSS.setSelectable(contents.getElement(), false);

		this.add(contents);
		this.getElement().getStyle().setProperty("zIndex", "" + ZDepth);

		contents.setPixelSize(ScaledSizeX, ScaledSizeY);

		this.setPixelSize(ScaledSizeX, ScaledSizeY);

		// update stats
		IDNumber = IDNumberSet;
		// Set Elememt
		this.getElement().setId("PID:" + IDNumber);

		setEditable(editable);

	}

	public void setEditable(boolean editable) {

		if (editable) {
			
			CHandel = this.addClickHandler(this);
			MDHandeler = this.addMouseDownHandler(this);
			MUHandeler = this.addMouseUpHandler(this);
			MMHandeler = this.addMouseMoveHandler(this);
			MOHandeler = this.addMouseOutHandler(this);
			
			//handle touch events here
			TSHandeler = this.addTouchStartHandler(this);
			TMHandeler = this.addTouchMoveHandler(this);
			TEHandeler = this.addTouchEndHandler(this);
			TCHandeler = this.addTouchCancelHandler(this);

		} else {

			if (CHandel != null) {
				CHandel.removeHandler();
				CHandel = null;
			}
			if (MDHandeler != null) {
				MDHandeler.removeHandler();
				MDHandeler = null;
			}
			if (MUHandeler != null) {
				MUHandeler.removeHandler();
				MUHandeler = null;
			}

			if (MMHandeler != null) {
				MOHandeler.removeHandler();
				MOHandeler = null;
			}
			if (MMHandeler != null) {
				MMHandeler.removeHandler();
				MMHandeler = null;
			}

			if (COHandeler != null) {
				COHandeler.removeHandler();
				COHandeler = null;
			}
			
			if (TSHandeler != null) {
				TSHandeler.removeHandler();
				TSHandeler = null;
			}
			if (TMHandeler != null) {
				TMHandeler.removeHandler();
				TMHandeler = null;
			}

			if (TEHandeler != null) {
				TEHandeler.removeHandler();
				TEHandeler = null;
			}
			if (TCHandeler != null) {
				TCHandeler.removeHandler();
				TCHandeler = null;
			}
			

		}
	}

	public void setBorders(boolean value) {

		if (BordersOn == value) {
			return;
		} else {
			BordersOn = value;
		}

		if (value) {
			this.remove(contents);

			// set to right type
			if (PanelType == PanelTypes.ECLIPSE) {
				Border = new SpiffyBox(SizeX, SizeY, SpiffyBox.elipseMode);
			} else {
				Border = new SpiffyBox(SizeX, SizeY, SpiffyBox.boxMode);

			}
			Frame.add(Border);
			Frame.add(contents, 0, 0);
			// add border

			this.add(Frame);
		} else {
			this.remove(Frame);
			this.add(contents);
		}

	}

	@Override
	public void setWidth(String newWidth) {
		contents.setWidth(newWidth);

	}

	@Override
	public void setHeight(String newHeight) {
		contents.setHeight(newHeight);

	}

	public void setAction(ComicPanelAction panelsAction) {
		this.panelsAction = panelsAction;

	}

	public void setLinkToAction(String link){	
		
		if (link==null||link.isEmpty()){
			this.setAction(null);
			return;
		}
		
	if ((link.toLowerCase().startsWith("http:"))
			|| (link.toLowerCase().startsWith("www."))) {

		Log.info("setting goto url action for panel");

		this.setAction(new GotoURL(link));

	} else {

		Log.info("setting goto location");

		this.setAction(new GotoLocation(link));

	}
	
	
	}
	
	@Override
	public void setPixelSize(int newWidth, int newHeight) {
		this.setSize(newWidth + "px", newHeight + "px");

		contents.setWidth(newWidth + "px");
		contents.setHeight(newHeight + "px");

		// set new scaled sizes (used by edge detection amongst other things)
		ScaledSizeX = newWidth;
		ScaledSizeY = newHeight;

		// if its a thread then resize
		if (PanelType == PanelTypes.THREAD) {

			// Window.setTitle("\n resizeing thread");

			((SpiffyLine) (contents)).resize(newWidth, newHeight);

		}

		System.out.print("\n PT=" + PanelType);

		if (PanelType == PanelTypes.TEXTBOX) {
			// System.out.print("\n resizing text");
			Label text = (Label) (((VerticalPanel) (contents)).getWidget(0));
			DOM.setStyleAttribute(text.getElement(), "fontSize",
					panelstreamer.GlobalZoom + "%");

		}

		// fix borders
		if (BordersOn) {
			// System.out.print("\n resizing borders");

			Border.resize(newWidth, newHeight);
		}
	}

	/** gets the new info of the panel **/
	public PanelInfo getInfoData() {

		// get the co-ordinates
		// get location in parent
		AbsolutePanel Parent =ParentPanel; //(AbsolutePanel) this.getParent();

		int CurrentX = getCurrentX();
		int CurrentY = getCurrentY();

		// get the data
		String DataURL = "(not supported yet)";
		String TextData = "";
		if (PanelType == PanelTypes.IMAGE) {
			DataURL = ((Image) contents).getUrl();
		}
		if (PanelType == PanelTypes.TEXTBOX) {
			TextData = ((TextPanelContent) contents).getText();
		}
		
		PanelInfo fullinfo = panelstreamer.AllComicPanelsInfo.GetByID(IDNumber);
		int onLayerNum = fullinfo.onLayerNumber;
		int zIndex = getCurrentZ();
		
		
		// package it
		PanelInfo panelInfo = new PanelInfo(IDNumber, PanelType, DataURL,
				TextData, CurrentX, CurrentY, zIndex, SizeX, SizeY, linkUrl,
				onLayerNum);

		//if its a thread, its details stay the same
		if (PanelType == PanelTypes.THREAD){
			
			//later this will have to be changed to match the enclosed thread
			panelInfo.DrawTo = fullinfo.DrawTo;
						
			panelInfo.DrawFrom = fullinfo.DrawFrom;
			panelInfo.DrawColour = fullinfo.DrawColour;
			
		}
		
		return panelInfo;
	}

	public int getCurrentZ() {
		PanelInfo fullinfo = panelstreamer.AllComicPanelsInfo.GetByID(IDNumber);
		return fullinfo.LocationZ;
	}

	public int getCurrentY() {
		return (int) Math.round(ParentPanel.getWidgetTop(this)
				/ (panelstreamer.GlobalZoom / 100));
	}

	public int getCurrentX() {
		return (int) Math.round(ParentPanel.getWidgetLeft(this)
				/ (panelstreamer.GlobalZoom / 100));
	}
	
	public void updateData(PanelInfo newData) {
		updateData(newData,true);
		
	}
	/** updates the panel with new data **/
	public void updateData(PanelInfo newData, boolean updatePosition) {

		AbsolutePanel parent = ParentPanel;//(AbsolutePanel) this.getParent();

		Double scalefactor = (panelstreamer.GlobalZoom / 100);
		
		PanelInfo oldData = panelstreamer.AllComicPanelsInfo.GetByID(newData.ID);
		
		if (updatePosition){
		// display new data
		int DisplayX = (int) Math.round(newData.LocationX * scalefactor);
		int DisplayY = (int) Math.round(newData.LocationY * scalefactor);
		parent.setWidgetPosition(this, DisplayX, DisplayY);
		}
		

		if (PanelType == PanelTypes.IMAGE) {
			
			//adjust for current quality
			String imageURL = newData.urlData.replace(
					"_Quality_", panelstreamer.currentQuality);
			//
			((Image) contents).setUrl(imageURL);
			
		}
		if (PanelType == PanelTypes.TEXTBOX) {
			((TextPanelContent) contents).setHTML(newData.TextString);
			//Text =
		}
		
		if (PanelType == PanelTypes.THREAD){
			//update colour
			((ThreadPanelContent) contents).setColour(newData.DrawColour);
			//only update the positions if they arnt set to null
			if (newData.DrawFrom ==null){
				//use old data
				newData.DrawFrom  = oldData.DrawFrom;
			}
			if (newData.DrawTo ==null){
				//use old data
				newData.DrawTo  = oldData.DrawTo;
			}
			//update the look of the thread
			
			
		}
		
		//update link
		if (newData.LinkTo!=null && !newData.LinkTo.isEmpty()){
			setLinkToAction(newData.LinkTo);
		}
		
		
		
		Log.info("scale factor_" + scalefactor);

		int ScaledX = (int) (newData.SizeX * scalefactor);
		int ScaledY = (int) (newData.SizeY * scalefactor);
		Log.info("unscaled = " + newData.SizeX + "," + newData.SizeY);
		Log.info("scaled = " + ScaledX + "," + ScaledY);

		this.setPixelSize(ScaledX, ScaledY);
		//update original size 
		SizeX = newData.SizeX;
		SizeY = newData.SizeY;
		
		// if (PanelType == PanelTypes.THREAD){
		// DataURL = "not supported yet - thread";
		// }

		//change layers
		//ComicPanel changeThis=panelstreamer.AllComicPanels.GetByID(newData.ID);
		
		//remove from old
		Iterator<ComicPanelLayer> lit = panelstreamer.layers.iterator();
		while (lit.hasNext()) {
			
			ComicPanelLayer comicPanelLayer = (ComicPanelLayer) lit.next();
			comicPanelLayer.removePanel(this);
		}
		
		//add to new
		panelstreamer.layers.get(newData.onLayerNumber).addPanel(this);
				
		//change zIndex
		panelsZDepth=getCurrentZ();
		
		this.getElement().getStyle().setZIndex(panelsZDepth);
		
		// store data
		Log.info("updating data:" + newData.ID+" "+newData.SizeX);
		panelstreamer.AllComicPanelsInfo.updatePanelInfo(newData.ID, newData);
		panelstreamer.AllLoadedComicPanelsInfo.updatePanelInfo(newData.ID,
				newData);

		return;
	}

	public void setEditModeOn(boolean bool) {
		
		Log.info("_________changing panel beingedit status");
		
		this.EDITMODE = bool;

		if (EDITMODE) {
			Log.info("_________ beingedited mode  on");
			// this.getElement().getStyle().setBorderStyle(Style.BorderStyle.SOLID);
			// this.getElement().getStyle().setBorderWidth(3, Unit.PX);
			// this.getElement().getStyle().setBorderColor("RED");

			this.addStyleName("PanelBeingEdited");

			if (COHandeler == null) {
				Log.info("add handler....");
				COHandeler = this.addDomHandler(this,
						ContextMenuEvent.getType());
				Log.info("added handler");
			}
			panelstreamer.infoPanel.setData(this.getInfoData());
			Log.info("set data ");

		} else {
			Log.info("_________ edit mode  off");

			this.removeStyleName("PanelBeingEdited");

			// this.getElement().getStyle().setBorderStyle(Style.BorderStyle.NONE);

			if (COHandeler != null) {
				Log.info("remove handler....");
				COHandeler.removeHandler();
				COHandeler = null;
				Log.info("removed.");
			}

		}

	}

	@Override
	public void onMouseUp(MouseUpEvent event) {
		onMouseOrTouchUp();
	}

	private void onMouseOrTouchUp() {
		Log.info("mouseup panel...");
		if (panelstreamer.EDITMODE) {

			// if (panelstreamer.PanelBeingEdited != null){
			// panelstreamer.PanelBeingEdited.setEditModeOn(false);
			// }

			this.updateData(this.getInfoData());

			DOM.releaseCapture(this.getElement());
			panelstreamer.PanelBeingMoved = false;

			// dont disable editing, as we might want to change its other
			// propertys
			// panelstreamer.PanelBeingEdited = null;

			BEINGDRAGED = false;
		}
		// add code to dragpanel to detect a mouseup while dragging an element.
	}

	@Override
	public void onMouseMove(MouseMoveEvent event) {

		if (panelstreamer.EDITMODE) {
			//Log.info("mouse move ...");
			if (BEINGDRAGED) {

				//Log.info("being dragged...");
				// get co-ordinates relative to parent frame (kinda messy)

				/*
				int x = event.getRelativeX((((AbsolutePanel) this.getParent())
						.getElement()));
				
				int y = event.getRelativeY((((AbsolutePanel) this.getParent())
						.getElement()));
*/
				
				//ParentPanel
				int x = event.getRelativeX(ParentPanel.getElement());				
				int y = event.getRelativeY(ParentPanel.getElement());
				
				
				onMouseOrTouchMove(x, y);

			}
		}
	}

	private void onMouseOrTouchMove(int x, int y) {
		
		//Log.info("_______mousemove detected in panel...x="+ x+"-"+panelstreamer.PanelBeingEdited.dragStartX+"="+(x - panelstreamer.PanelBeingEdited.dragStartX));
        
		panelstreamer.ContainerPanel.movePanel(this, x - dragStartX, y
				- dragStartY);
		
		panelstreamer.infoPanel.setData(this.getInfoData());
		
	}

	@Override
	public void onMouseDown(MouseDownEvent event) {
		
		int x = event.getX();
		int y = event.getY();
		
		onMouseOrTouchDown(x, y);
	}

	private void onMouseOrTouchDown(int x, int y) {
		if (panelstreamer.EDITMODE) {
			Log.info("mousedown on panel...");
			// remove any existing panels being edited
			if (panelstreamer.PanelBeingEdited != null) {
				panelstreamer.PanelBeingEdited.setEditModeOn(false);
			}
			Log.info("start editing panel...");

		
			Log.info("drag start a " + x + " y=" + y);

			dragStartX = x - 0;
			dragStartY = y - 0;

			panelstreamer.PanelBeingEdited = this;
			panelstreamer.PanelBeingMoved = true;
			panelstreamer.PanelBeingEdited.setEditModeOn(true);

			DOM.setCapture(this.getElement());
			Log.info("capture set...");
			BEINGDRAGED = true;
		}
	}

	@Override
	public void onClick(ClickEvent event) {

		// Log.info("clicked on panel...");

		// If the panelstreamer GUI has edit mode on, then we set the panel to
		// edit mode;
		if (panelstreamer.EDITMODE) {

			// remove any existing panels being edited
			if (panelstreamer.PanelBeingEdited != null) {
				panelstreamer.PanelBeingEdited.setEditModeOn(false);
			}

			Log.info("start editing panel...");
			setEditModeOn(true);
			panelstreamer.PanelBeingEdited = this;

			Log.info("editing setup...");

		} else {
			// Else we run the panels normal click commands, if it has any.
			if (panelsAction != null) {
				Log.info("triggering action...");

				panelsAction.triggerAction();
			}
		}

	}

	@Override
	public void onMouseOut(MouseOutEvent event) {

		if (BEINGDRAGED) {
			Log.info("mouse out of panel...");
			DOM.releaseCapture(this.getElement());
			// panelstreamer.PanelBeingMoved=false;

			// int x =
			// event.getRelativeX((((AbsolutePanel)this.getParent()).getElement()));
			// int y =
			// event.getRelativeY((((AbsolutePanel)this.getParent()).getElement()));

			// panelstreamer.ContainerPanel.movePanel(this, x-dragStartX,
			// y-dragStartY);

		}
		BEINGDRAGED = false;
	}

	/**
	 * Override the standard Image functionalities onBrowserEvent method to
	 * ensure that the default action of the browser for an image is not taken -
	 * i.e. it allows us to cleanly drag the image around.
	 */
	@Override
	public void onBrowserEvent(Event e) {
		if (e.getButton()!= NativeEvent.BUTTON_RIGHT) {

			if (e.getTypeInt() !=  Event.ONMOUSEMOVE) {
			//Log.info("non-right non-movement click dom event triggered");
			}
			
			DOM.eventPreventDefault(e);
			super.onBrowserEvent(e);

		} else {
			
			Log.info("right click event triggered");
			if (panelstreamer.EDITMODE) {

				DOM.eventPreventDefault(e);
				super.onBrowserEvent(e);

			}
		}
	}

	@Override
	public void onContextMenu(ContextMenuEvent event) {

		Log.info("context menu event triggered");

		// override the menu only in edit mode
		if (panelstreamer.EDITMODE) {
			event.stopPropagation(); // This will stop the event from being
										// propagated to parent elements.
			
			event.preventDefault(); // because cancelBubble(Boolean) is
									// deprecated

			
			
			ContextMenuPopUpOptions popupMenu = new ContextMenuPopUpOptions();
			popupMenu.setAutoHideEnabled(true);
			popupMenu.setPopupPosition(event.getNativeEvent().getClientX(),
					event.getNativeEvent().getClientY());
			popupMenu.show();

		} else {
			
			if (disableFocus){
				event.preventDefault();
			}
			Log.info("_________context menu event triggered but no edit mode");
		}
	}

	@Override
	public void onTouchCancel(TouchCancelEvent event) {
		onMouseOrTouchUp();
	}

	@Override
	public void onTouchEnd(TouchEndEvent event) {
		onMouseOrTouchUp();
		
	}

	@Override
	public void onTouchMove(TouchMoveEvent event) {
		
		if (panelstreamer.EDITMODE) {
			//Log.info("mouse move ...");
			if (BEINGDRAGED) {

				//Log.info("being dragged...");
				// get co-ordinates relative to parent frame (kinda messy)

				int x = event.getTouches().get(0).getRelativeX(((ParentPanel.getElement())));
				
				int y = event.getTouches().get(0).getRelativeY(((ParentPanel.getElement())));

				onMouseOrTouchMove(x, y);

			}
		}
		
	}

	@Override
	public void onTouchStart(TouchStartEvent event) {
		
		int x = event.getTouches().get(0).getRelativeX(event.getRelativeElement());
		int y = event.getTouches().get(0).getRelativeY(event.getRelativeElement());
		
		
		onMouseOrTouchDown(x, y);
	}

}
