package com.darkflame.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;


public class GlobalPanelStore {

	 Logger Log = Logger.getLogger("GlobalPanelStoreLog");
	 
	//final static ArrayList <ComicPanel> AllPanels = new ArrayList<ComicPanel>();
	final static HashMap <Integer,ComicPanel> AllPanels = new HashMap <Integer,ComicPanel>();
	
	public GlobalPanelStore(){
		
	}
	
	public void clear() {
		AllPanels.clear();
	}
		
	
	
	public void add(ComicPanel newcomicpanel){
		
		Log.info("_________adding id"+newcomicpanel.IDNumber);
		
		AllPanels.put(new Integer(newcomicpanel.IDNumber),newcomicpanel);
		
		Log.info("______________added id:"+newcomicpanel+" with :"+ AllPanels.get(new Integer(newcomicpanel.IDNumber)).linkUrl );
	}
	
	public void remove(ComicPanel newcomicpanel){
		Log.info("_________removing id"+newcomicpanel.IDNumber);
		AllPanels.remove(new Integer(newcomicpanel.IDNumber));
	}

	public Iterator<ComicPanel> GetPanelIterator(){
		
		Log.info("RETURNING ITERATOR FOR: "+AllPanels.values().size()+" panels");
		
		Iterator<ComicPanel> it = AllPanels.values().iterator();;
		
		return it;
	}
	public ComicPanel GetByID(int IDrequested){

		Log.info("getting by id"+IDrequested);
		
		ComicPanel ThisPanel = AllPanels.get(new Integer(IDrequested));
		
		//Log.info("got by id?"+ThisPanel.IDNumber);
		
		/*
		Iterator<ComicPanel> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			ComicPanel CurrentPanel = panelit.next();
			
			if (CurrentPanel.IDNumber == IDrequested){
				ThisPanel = CurrentPanel;
			}
			
		}*/
		
		
		return ThisPanel;
	}
}
