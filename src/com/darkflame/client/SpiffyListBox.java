package com.darkflame.client;

import java.util.ArrayList;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SpiffyListBox extends SimplePanel  {
	
	
	private static final String Default_Label_Style = "gwt-Button";
	
	
	DisclosurePanel DropDownContainer = new DisclosurePanel();
	public VerticalPanel ListContainer = new VerticalPanel();
	public ScrollPanel ScrollContainer = new ScrollPanel();
	ArrayList<String> FieldNames = new ArrayList<String>();
	ArrayList<String> FieldValues = new ArrayList<String>();
	 ArrayList<Label> FieldLabels = new ArrayList<Label>();
		
	 Label CurrentSelectedLab = new Label("  ");
     int CurrentSelectNum =0;
	private int visibleitems;
	private ChangeHandler onSelected;
	
	public String hoverStyleName = "SpiffylistHoverStyle";
	
	public SpiffyListBox(){
		
		super.setWidget(DropDownContainer);
					
		DropDownContainer.setHeader(CurrentSelectedLab);
		CurrentSelectedLab.setStylePrimaryName("TitleLabel");
		
		ScrollContainer.add(ListContainer);
		
		DropDownContainer.setContent(ScrollContainer);
		
		//default style
		DropDownContainer.setStylePrimaryName("SpiffyTextBox");
		
	}

	public void addItem (String name){
		addItem(name, null);
	}
	public void addItem(String name, String value){
		
		if (FieldNames.size()==0){
			CurrentSelectedLab.setText(name);
			CurrentSelectNum=0;
		}
		
		selectableLabel newLabel = new selectableLabel(name);
		newLabel.setStylePrimaryName(Default_Label_Style);
		
		FieldNames.add(name);
		FieldValues.add(value);
		FieldLabels.add(newLabel);
		
		ListContainer.add(newLabel);
		
		
	}
	/** note; you can set it to 1, or more then 1, but more then 1 currently acts as infinite **/
	public void setVisibleItemCount(int num){
		visibleitems = num;
		if (num>1){
			
			DropDownContainer.setOpen(true);
			//dont show the header when on this mode.
			CurrentSelectedLab.setVisible(false);
			//remove disclosure style
			DropDownContainer.removeStyleName("content");
			
		}
	}
	
	private class selectableLabel extends Label {
		private final class SelectableLabelHandeler implements
				ClickHandler {			
			public void onClick(ClickEvent event) {
				//get sender
				Label current = (Label) event.getSource();					
				int Position = FieldLabels.indexOf(current);
						
				
				
				
				//Set current selected
				CurrentSelectedLab.setText(current.getText());
				CurrentSelectNum=Position;
				
				//autoclose
				if (visibleitems==1){
				DropDownContainer.setOpen(false);
				}
				
				//trigger change
				if (onSelected!=null){
				onSelected.onChange(null);
				}
				
			}
		}
		private final class SelectableLabelMouseOverHandeler implements
		MouseOverHandler {			
			public void onMouseOver(MouseOverEvent event) {
				Label current = (Label) event.getSource();	
				//Set style to ollover
				current.addStyleName(hoverStyleName);
		}
		}
			private final class SelectableLabelMouseOutHandeler implements
			MouseOutHandler {			
				public void onMouseOut(MouseOutEvent event) {
					Label current = (Label) event.getSource();	
					//Set style to ollover
					current.removeStyleName(hoverStyleName);
			}
	}


		public selectableLabel(String name){
			
			super.setText(name);
						
			super.addClickHandler(new SelectableLabelHandeler());
			super.addMouseOverHandler(new SelectableLabelMouseOverHandeler());
			super.addMouseOutHandler(new SelectableLabelMouseOutHandeler());
			
		}
	}
	
	@Override	
	public void clear() {
		FieldNames.clear();
		FieldValues.clear();
		FieldLabels.clear();		
		ListContainer.clear();
		CurrentSelectNum=0;
		
		  }
	
	public int getSelectedIndex(){
		return CurrentSelectNum;
	}
	public String getItemText(int Item){
		return FieldNames.get(Item);
	}
	public String getValue(int Item){
		return FieldValues.get(Item);
	}

	public void setFocus(boolean value){
		//
	}
	public void setItemSelected(int ItemNum, boolean dosntEffectThisBox){
		setItemSelected(ItemNum);
	}
	public void setItemSelected(int ItemNum){
		CurrentSelectNum=ItemNum;
		
	//	Window.alert("selecting item"+CurrentSelectNum);
		
		
		if (visibleitems==1){
			CurrentSelectedLab.setText( FieldNames.get(CurrentSelectNum) );
		}
		
	}
	public int getItemCount(){
		return FieldNames.size();
	}

	public void setWidth(String width){
		super.setWidth(width);
		ListContainer.setWidth(width);
	}
	
	public void addChangeHandler(ChangeHandler changeHandler) {
	
		onSelected = changeHandler;
		
	}
}
