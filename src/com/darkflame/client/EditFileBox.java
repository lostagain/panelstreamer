package com.darkflame.client;
import java.util.Iterator;

import com.darkflame.client.ComicPanel.PanelTypes;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class EditFileBox extends PopupPanel {
	
	TextArea XMLContents = new TextArea();
	VerticalPanel Contents = new VerticalPanel();	
	
	HorizontalPanel Update_or_Cancel = new HorizontalPanel();
	Button Update = new Button("Update");
	Button Cancel = new Button("Cancel");
	//--
	EditFileBox updateXMLBox = this;
	
	public EditFileBox(){
		
		this.setModal(true);
		this.setAutoHideEnabled(true);
		
		//set to a high level of zIndex
		updateXMLBox.getElement().getStyle().setProperty("zIndex", "99999");
		
		Update_or_Cancel.add(Update);
		Update_or_Cancel.add(Cancel);
		XMLContents.setSize("550px", "450px");
		
		Contents.add(XMLContents);
		Contents.add(Update_or_Cancel);
		Contents.setCellHorizontalAlignment(Update_or_Cancel,HasHorizontalAlignment.ALIGN_CENTER);
		
		this.add(Contents);
		
		Cancel.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				updateXMLBox.hide();
			}			
		});
		
		Update.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				//store current position
				
				panelstreamer.StartAtX = panelstreamer.currentXpos;
				panelstreamer.StartAtY = panelstreamer.currentYpos;
				panelstreamer.StartAtZoom = (int)panelstreamer.GlobalZoom;
				panelstreamer.UpdatePositionAfterLoad = true;
				panelstreamer.clearExistingData();
				panelstreamer.loadXMLdata("(edited)", false, XMLContents.getText());
				
				//panelstreamer.GlobalZoom=zoom;
				//panelstreamer.UpdatePosition(x, y);
				
				
				
				updateXMLBox.hide();
			}			
		});
		
		
	}
	@Override
	public void center(){
		
		String XMLdata = fetchXMLdata();
		
		
		
		
		XMLContents.setText(XMLdata);
		
		super.center();
		
	}
	
	
	public String fetchXMLdata(){
		/*
		Iterator<ComicPanel> it = GlobalPanelStore.AllPanels.iterator();
		
		
		while (it.hasNext()){
			data = data + "Type="+it.next().PanalType+" \n";
			
		}
		 Iterator<PanelInfo> panelsIt = panelstreamer.AllComicPanelsInfo.AllPanels.iterator();
		 
		while (panelsIt.hasNext()){
			data = data + ".Type="+panelsIt.next().Type+" \n";
				
		}
		*/
		String data = "<Collection name=\""+panelstreamer.CurrentPanelCollectionName+"\">\r\n";
		// Loop for each layer
		
		Iterator<layerInfo> LayersPanels = panelstreamer.layersinfo.iterator();

		int layernum=0;
		
		while (LayersPanels.hasNext()){
			
			layerInfo LayersData = LayersPanels.next();
			
			data = data + "<layer name=\""+LayersData.Title+"\" >\r\n";
			//data = data + "\r\n Z="+LayersData.zDepth;
			//data = data + "\r\n title="+LayersData.Title;
						
			
			//get the panels on the layer
			//Iterator<ComicPanel> panelsIt = panelstreamer.layers.get(layernum).comicPanelsOnLayer.iterator();
			
			Iterator<PanelInfo> panelsIt = panelstreamer.AllComicPanelsInfo.getAllOnLayer(layernum).iterator();
					
			
			// Iterator<PanelInfo> panelsIt = panelstreamer.AllLayersPanelInfo.get(layernum).iterator();
			 
			 while (panelsIt.hasNext()){
				 
				 PanelInfo  currentPanelInfo = panelsIt.next();
				 
				// ComicPanel currentPanel = panelsIt.next();
				// PanelInfo  currentPanelInfo = panelstreamer.AllComicPanelsInfo.GetByID(currentPanel.IDNumber);
				 				 
				 
				 //data bits
				 String LinkToBit="";
				 
				 if (currentPanelInfo.LinkTo != null){
					 LinkToBit = " LinkURL=\""+currentPanelInfo.LinkTo.replace("&", "&amp;") +"\" ";
					 
				 }
				 
				 //data (will need to be updated when xml format changes)
				 //possibly refractor into comicpanel info function
				 
				 //fix data if its thread type
				 if (currentPanelInfo.Type == PanelTypes.THREAD){
					 data = data + "<panel ID=\""+currentPanelInfo.ID+"\" Data=\"Thread=Col:"+currentPanelInfo.DrawColour+";FromID:"+currentPanelInfo.DrawFrom.PanelID+";ToID:"+currentPanelInfo.DrawTo.PanelID+"\">";
					 
				 } else if  (currentPanelInfo.Type == PanelTypes.TEXTBOX) {
					 
					 //full encoding probably not needed - just escape "'s 
					 String escapedText =  URL.encode(currentPanelInfo.TextString);
					 
					 data = data + "<panel ID=\""+currentPanelInfo.ID+"\" Data=\""+escapedText+"\" ";
						
					 //size 
						data = data + "SizeX=\""+currentPanelInfo.SizeX+"\" SizeY=\""+currentPanelInfo.SizeY+"\" ";
					 //pos 
						data = data + "LocX=\""+currentPanelInfo.LocationX +"\" LocY=\""+currentPanelInfo.LocationY+"\" LocZ=\""+currentPanelInfo.LocationZ+"\""+LinkToBit+">";
					
					 
					 
				 } else {
					 data = data + "<panel ID=\""+currentPanelInfo.ID+"\" Data=\""+currentPanelInfo.urlData+"\" ";
						
					 //size 
						data = data + "SizeX=\""+currentPanelInfo.SizeX+"\" SizeY=\""+currentPanelInfo.SizeY+"\" ";
					 //pos 
						data = data + "LocX=\""+currentPanelInfo.LocationX +"\" LocY=\""+currentPanelInfo.LocationY+"\" LocZ=\""+currentPanelInfo.LocationZ+"\""+LinkToBit+">";
						
				 }
				 
				 
				
				
					
					
					data=data+"</panel>\r\n";
						
				}
		// Loop for each object within the layer
		
		//
			 data = data + "</layer>\r\n";
			 layernum++;
		}
		//
		data = data + "</Collection>\r\n";
		return  data;
	}
	
	
}
