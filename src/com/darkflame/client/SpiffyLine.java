package com.darkflame.client;


import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.SimplePanel;

//import gwt.canvas.client.Canvas;

public class SpiffyLine extends SimplePanel {
	
	//this is a class to draw lines from one corner to another.
	
//	Canvas main = new Canvas();
	Canvas mainc = Canvas.createIfSupported();
	CanvasElement maine = mainc.getCanvasElement();
	Context2d main = maine.getContext2d();
	
	int StartX, StartY = 0;
	
	int cStartX, cStartY = 0;
	int cMiddleX, cMiddleY = 0;
	int cEndX, cEndY = 0;
	int width = 4; /** line width **/
	int FROMcode = 0;
	int TOcode = 0;
	
	String colour;
	/**positions are from top left like thus
		 ------------
		   1    2     3
		 12			   4 	
		
		 11			   5	
		
		 10			   6	
		   9    8     7
		**/ 
	public SpiffyLine(int SizeX, int SizeY, String Colour, int FromPos /* comment */, int ToPos){
		FROMcode = FromPos;
		TOcode = ToPos;
		colour = Colour;
		
		
		
		
	

		
		this.setPixelSize(SizeX, SizeY);
		//main.setPixelSize(SizeX, SizeY);
		mainc.setPixelSize(SizeX, SizeY);
		mainc.setCoordinateSpaceHeight(SizeY);
		mainc.setCoordinateSpaceWidth(SizeX);
		width = (int)(6*(panelstreamer.GlobalZoom/100));
		
		this.add(mainc);
		//main. .setBackgroundColor(  );
		
		//Canvas.TRANSPARENT);

		calculateStartPosition(SizeX, SizeY, FromPos);		
		calculateMiddleAndEndPositions(SizeX, SizeY, ToPos);
		
		drawPath();
		
		
	}
	
	public void setColour(CssColor newCol){
		colour = newCol.value();
		//redraw
		drawPath();
		
	}

	private void drawPath() {
		main.beginPath();
		main.setStrokeStyle(colour);

		System.out.print("______________"+colour);
		
		
		main.setLineWidth(width);
		
		//get start position
		
		
	//	System.out.print("\n drawing curve from "+cStartX+" "+cStartY);
		//System.out.print("\n drawing curve to "+cMiddleX+" "+cMiddleY);
	//	System.out.print("\n drawing curve to "+cEndX+" "+cEndY);
		main.moveTo(StartX, StartY);
		//main.cubicCurveTo(cStartX, cStartY, c2StartX, c2StartY, cEndX, cEndY);
		main.bezierCurveTo(cStartX, cStartY, cMiddleX, cMiddleY, cEndX, cEndY);
		
		//main.rect(0, 0, SizeX, SizeY);
		main.stroke();
	}
	
public void resize (int SizeX, int SizeY){
	
//	Window.setTitle("\n resizeing thread 2 " + SizeX+" "+SizeY);
	
	
	this.setPixelSize(SizeX, SizeY);
	
	
	//note: hangs here;
	
	try {
		//Window.setTitle("\n resizeing thread 3 " + SizeX+" "+SizeY);
		//main.setPixelSize(SizeX, SizeY);
		mainc.setPixelSize(SizeX, SizeY);
		mainc.setCoordinateSpaceHeight(SizeY);
		mainc.setCoordinateSpaceWidth(SizeX);
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//Window.setTitle("\n cant resize 3 " + SizeX+" "+SizeY);
		Window.alert("cant set pixal size of canvas to;"+SizeX+" "+SizeY);
		
	}	

	//Window.setTitle("\n resizeing thread 4" + SizeX+" "+SizeY);
	
	main.beginPath();
	main.setStrokeStyle(colour);
	
	width = (int)(6*(panelstreamer.GlobalZoom/100));
	main.setLineWidth(width);
	
	
	
	//get start position
	
	calculateStartPosition(SizeX, SizeY, FROMcode);		
	
	calculateMiddleAndEndPositions(SizeX, SizeY, TOcode);
	
	System.out.print("\n drawing curve from "+cStartX+" "+cStartY);
	System.out.print("\n drawing curve to "+cMiddleX+" "+cMiddleY);
	System.out.print("\n drawing curve to "+cEndX+" "+cEndY);
	main.moveTo(StartX, StartY);
	//main.cubicCurveTo(cStartX, cStartY, cMiddleX, cMiddleY, cEndX, cEndY);
	main.bezierCurveTo(cStartX, cStartY, cMiddleX, cMiddleY, cEndX, cEndY);
	
	//main.rect(0, 0, SizeX, SizeY);
	
	
	main.stroke();
}
	
	private void calculateMiddleAndEndPositions(int SizeX, int SizeY, int ToPos) {
		int CurveDisplacementX = SizeX/4;
		int CurveDisplacementy = SizeY/4;
		
		
		if (ToPos == 1){		
			cMiddleX = width/2;
			cMiddleY = CurveDisplacementy;
			cEndX = width/2;
			cEndY = 0;
			
		}
		if (ToPos == 2){
			
			cMiddleX = Math.round(SizeX/2);
			cMiddleY = CurveDisplacementy;
			cEndX = Math.round(SizeX/2);
			cEndY = 0;
		}
		if (ToPos == 3){
			
			cMiddleX = SizeX-4;
			cMiddleY = CurveDisplacementy;
			cEndX = SizeX-width/2;
			cEndY = 0;
		}
		//get end pos
		if (ToPos == 4){		
			cMiddleX = SizeX-CurveDisplacementX;
			cMiddleY = width/2;
			cEndX = SizeX;
			cEndY = width/2;
			
		}
		if (ToPos == 5){
			
			cMiddleX = SizeX-CurveDisplacementX;
			cMiddleY = Math.round(SizeX/2);
			cEndX = SizeX;
			cEndY = Math.round(SizeX/2);
		}
		if (ToPos == 6){
			
			cMiddleX = SizeX-CurveDisplacementX;
			cMiddleY = SizeY-width/2;
			cEndX = SizeX;
			cEndY = SizeY-width/2;
		}
		if (ToPos == 9){		
			cMiddleX = width/2;
			cMiddleY = SizeY-CurveDisplacementy;
			cEndX = width/2;
			cEndY = SizeY;
			
		}
		if (ToPos == 8){
			
			cMiddleX = Math.round(SizeX/2);
			cMiddleY = SizeY-CurveDisplacementy;
			cEndX = Math.round(SizeX/2);
			cEndY = SizeY;
		}
		if (ToPos == 7){
			
			cMiddleX = SizeX-width/2;
			cMiddleY = SizeY-CurveDisplacementy;
			cEndX = SizeX-width/2;
			cEndY = SizeY;
		}
		//get end pos
		if (ToPos == 12){		
			cMiddleX = CurveDisplacementX;
			cMiddleY = width/2;
			cEndX = 0;
			cEndY = width/2;
			
		}
		if (ToPos == 11){
			
			cMiddleX = CurveDisplacementX;
			cMiddleY = Math.round(SizeX/2);
			cEndX = 0;
			cEndY = Math.round(SizeX/2);
		}
		if (ToPos == 10){
			
			cMiddleX = CurveDisplacementX;
			cMiddleY = SizeY-width/2;
			cEndX = 0;
			cEndY = SizeY-width/2;
		}
	}

	private void calculateStartPosition(int SizeX, int SizeY, int FromPos) {
		int CurveDisplacementX = SizeX/4;
		int CurveDisplacementy = SizeY/4;
		
		
		if (FromPos == 1){
			
			//main.moveTo(4, 0);
			StartX = width/2;
			StartY = 0;
			cStartX = width/2;
			cStartY = CurveDisplacementy;
		}
		if (FromPos == 2){
			//main.moveTo( Math.round(SizeX/2), 0);
			StartX = Math.round(SizeX/2);
			StartY = 0;
					
			cStartX = Math.round(SizeX/2);
			cStartY = CurveDisplacementy;
		}
		if (FromPos == 3){
			//main.moveTo(SizeX-4, 0);
			StartX = SizeX-width/2;
			StartY = 0;
			
			
			cStartX = SizeX-width/2;
			cStartY = CurveDisplacementy;
		}
if (FromPos == 4){
			
			//main.moveTo(SizeX, 4);
			StartX = SizeX;
			StartY = width/2;
			
			cStartX = SizeX-CurveDisplacementX;
			cStartY = width/2;
		}
		if (FromPos == 5){
			//main.moveTo( SizeX,  Math.round(SizeY/2));
			StartX = SizeX;
			StartY = Math.round(SizeY/2);
			
			cStartX =  SizeX-CurveDisplacementX;
			cStartY = Math.round(SizeY/2);
		}
		if (FromPos == 6){
			//main.moveTo(SizeX, SizeY-4);
			
			StartX = SizeX;
			StartY = SizeY-width/2;			
			
			cStartX = SizeX-CurveDisplacementX;
			cStartY =  SizeY-width/2;
		}
if (FromPos == 7){
			
			//main.moveTo( SizeX-4, SizeY);
	StartX = SizeX-width/2;
	StartY = SizeY;			
	
			cStartX = SizeX-width/2;
			cStartY = SizeY+CurveDisplacementy;
		}
		if (FromPos == 8){
			
			//main.moveTo( Math.round(SizeX/2),  SizeY);
			StartX = Math.round(SizeX/2);
			StartY = SizeY;			
			
			
			cStartX =  Math.round(SizeX/2);
			cStartY = SizeY+CurveDisplacementy;
		}
		if (FromPos == 9){
		//main.moveTo(4, SizeY);
			StartX = width/2;
			StartY = SizeY;			
			
			
			
			cStartX = width/2;
			cStartY = SizeY+CurveDisplacementy;
		}
		
if (FromPos == 12){
			
			//main.moveTo(0, 4);
	StartX = 0;
	StartY = width/2;			
	
	
	
			cStartX = CurveDisplacementX;
			cStartY = width/2;
		}
		if (FromPos == 11){
			//main.moveTo( 0,  Math.round(SizeY/2));
			StartX = 0;
			StartY = Math.round(SizeY/2);			
			
			
			cStartX = CurveDisplacementX;
			cStartY = Math.round(SizeY/2);
		}
		if (FromPos == 10){
			//main.moveTo(0, SizeY-4);
			StartX = 0;
			StartY = SizeY-width/2;			
			
			cStartX = CurveDisplacementX;
			cStartY = SizeY-width/2;
		}
	}

}
