package com.darkflame.client;

import java.util.HashMap;
import java.util.HashSet;

public class CollectionStats {

	
	static HashSet <String> ImageURLs = new HashSet <String>();	
	static HashMap <String, XYPoint> ImageData = new HashMap <String, XYPoint>();
	
	static HashSet <String> WebpageURLs = new HashSet <String>();	
	static HashSet <Integer> AllIDs = new HashSet <Integer>();
	
	//static HashSet <Integer> AllLayerNames = new HashSet <Integer>();
	
	static public void clearStats(){
		ImageURLs.clear();
		WebpageURLs.clear();
		AllIDs.clear();
		ImageData.clear();
		
	}

}
