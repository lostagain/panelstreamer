package com.darkflame.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;


public class PanelInfoStore {

	final HashMap <Integer,PanelInfo> AllPanels = new HashMap <Integer,PanelInfo>();

    static Logger Log = Logger.getLogger("PanelInfoStoreLog");
    
	public PanelInfoStore(){
		
	}
	public void clear() {
		AllPanels.clear();
	}
		
	public void add(PanelInfo newcomicpanelingo){
		
		Log.info("Adding :"+newcomicpanelingo.ID);
		
		AllPanels.put(newcomicpanelingo.ID,newcomicpanelingo);
	}
	
	public void remove(PanelInfo comicpanelinfo){
		AllPanels.remove(comicpanelinfo.ID);
	}

	public void updatePanelInfo(int IDrequested,PanelInfo newinfo){
		
		AllPanels.put(new Integer(IDrequested), newinfo);
		
	}
	
	public PanelInfo GetByID(int IDrequested){
		
		//Log.info("IDrequested :"+IDrequested);
		
		PanelInfo ThisPanel = AllPanels.get(new Integer(IDrequested));

		
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				ThisPanel = CurrentPanel;
			}
			
		}
		*/
		
		return ThisPanel;
	}
public void RemoveByID(int IDrequested){
		
		PanelInfo ThisPanel = AllPanels.get(IDrequested);
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				ThisPanel = CurrentPanel;
			}
			
		}
		*/
		
		AllPanels.remove(ThisPanel);
	}
	
	public Iterator<PanelInfo> getIterator(){		
		return AllPanels.values().iterator();
	}
	public boolean IDexists(int IDrequested){
		
		boolean state = AllPanels.containsKey(IDrequested);
		//Log.info("id exists:"+state);
		/*
		Iterator<PanelInfo> panelit = AllPanels.iterator();
		
		while(panelit.hasNext()){
			
			PanelInfo CurrentPanel = panelit.next();
			
			if (CurrentPanel.ID == IDrequested){
				state = true;
			}
			
		}*/
		
		
		return state;
	}
	
	
	public ArrayList<PanelInfo> getAllOnLayer(int LayerNum){
		
		ArrayList<PanelInfo> infoSet = new ArrayList<PanelInfo>();
		
		Iterator<PanelInfo> panelit = this.getIterator();
		
		while (panelit.hasNext()) {
			
			PanelInfo panelInfo = (PanelInfo) panelit.next();
			
			if (panelInfo.onLayerNumber == LayerNum){
				infoSet.add(panelInfo);
			}
			
		}
		
		
		return infoSet;
		
		
	}
	
	
}
