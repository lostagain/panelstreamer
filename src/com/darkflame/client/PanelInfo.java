package com.darkflame.client;

import java.util.logging.Logger;

import com.google.gwt.canvas.dom.client.CssColor;

public class PanelInfo {
	
	//String Type="";
	ComicPanel.PanelTypes Type;
	
	//overall data
	String urlData="";
	//bits that might make it up
	String TextString = "";
	
	int ID = 0;
	
	int LocationX = 0;
	int LocationY = 0;
	int LocationZ = 0; /** i.e, the zIndex if viewed on a webpage **/
	
	//int zIndex=1000; //not used use LocZ instead
		
	int SizeX = 0;
	int SizeY = 0;
	
	//link to and from for thread data
	enum PositionInPanel {
		TopLeft,TopMiddle,TopRight,MiddleLeft,MiddleRight,BottomLeft,BottomMiddle,BottomRight;
		
		/** returns the enum version of a given position string (case sensative, use CamelCase) **/
	
		public static PositionInPanel StringToPosition(String stringPosition) throws UnrecognisedPositionError {
						
			if (stringPosition.equals("TopLeft")){
				return PositionInPanel.TopLeft;
			}
			if (stringPosition.equals("TopRight")){
				return PositionInPanel.TopRight;
			}
			if (stringPosition.equals("BottomLeft")){
				return PositionInPanel.BottomLeft;
			}
			if (stringPosition.equals("BottomRight")){
				return PositionInPanel.BottomRight;
			}
			if (stringPosition.equals("MiddleLeft")){
				return PositionInPanel.MiddleLeft;
			}
			if (stringPosition.equals("MiddleRight")){
				return PositionInPanel.MiddleRight;
			}
			if (stringPosition.equals("TopMiddle")){
				return PositionInPanel.TopMiddle;
			}
			if (stringPosition.equals("BottomMiddle")){
				return PositionInPanel.BottomMiddle;
			} else {
				throw new UnrecognisedPositionError(stringPosition);
			}
			
			
			
		}
		
		static class UnrecognisedPositionError extends Exception {
			String PositionNotRecognised;
			public UnrecognisedPositionError(String PositionNotRecognised){
				this.PositionNotRecognised = PositionNotRecognised;
			}
			/**
			 * 
			 */
			private static final long serialVersionUID = -1949030652629948456L;
		}
	}
	PanelAndPosition DrawFrom=null;
	PanelAndPosition DrawTo=null;
	CssColor DrawColour=CssColor.make(0, 0, 0);
	
	/** specifys a preset position in a panel, eg "topleft of 15**/
	class PanelAndPosition {
		int PanelID;
		PositionInPanel PositionInPanel;
		
		public PanelAndPosition(int panelID,
				com.darkflame.client.PanelInfo.PositionInPanel positionInPanel) {
			super();
			PanelID = panelID;
			PositionInPanel = positionInPanel;
		}
		
		
		
	}
	
	//url link to trigger on click
	String LinkTo = "";
	
	//Its layer number
	int onLayerNumber = 0;
	//its z-index

	static Logger Log = Logger.getLogger("PanelInfo");
	
	public PanelInfo(int IDnum, ComicPanel.PanelTypes  setType, String LocData,String TextData, int LocX, int LocY, int LocZ, int SX, int SY, String LinkThisTo,int setLayerNumber){
		
		 urlData=LocData;
		 ID = IDnum;
		Type = setType;
		TextString = TextData;
		
		 LocationX = LocX;
		 LocationY = LocY;
		 LocationZ = LocZ;
	//	 this.zIndex = ZIndex;
		 
		 SizeX = SX;
		 SizeY = SY;
		
		 LinkTo = LinkThisTo;
		
		 onLayerNumber = setLayerNumber;
		 Log.info("__________________ "+IDnum+": layer set:"+onLayerNumber);
		 
	}
	
	public void setThreadDataByStrings(String Start,String End,String Color) throws PositionInPanel.UnrecognisedPositionError
	{
		//split to get num
		String PID = Start.split(";",2)[0];
		String sLOC = Start.split(";",2)[1].substring(4);
		int sPID = Integer.parseInt(PID);
		
		String PID2 = End.split(";",2)[0];
		String eLOC = End.split(";",2)[1].substring(5);
		int ePID = Integer.parseInt(PID2);
		
		
		//Log.info("_______setting position to ");
		//Log.info("_______"+PositionInPanel.StringToPosition(sLOC).ordinal());
		
		DrawFrom = new PanelAndPosition(sPID,PositionInPanel.StringToPosition(sLOC));
		DrawTo = new PanelAndPosition(ePID,PositionInPanel.StringToPosition(eLOC));	
		DrawColour = CssColor.make(Color);
		
	}
	
	
	/** returns the enum version of a given position string (case sensative, use CamelCase) 
	private PositionInPanel StringToPosition(String stringPosition) throws UnrecognisedPositionError {
		
		if (stringPosition.equals("TopLeft")){
			return PositionInPanel.TopLeft;
		}
		if (stringPosition.equals("TopRight")){
			return PositionInPanel.TopRight;
		}
		if (stringPosition.equals("BottomLeft")){
			return PositionInPanel.BottomLeft;
		}
		if (stringPosition.equals("BottomRight")){
			return PositionInPanel.BottomRight;
		}
		if (stringPosition.equals("MiddleLeft")){
			return PositionInPanel.MiddleLeft;
		}
		if (stringPosition.equals("MiddleRight")){
			return PositionInPanel.MiddleRight;
		}
		if (stringPosition.equals("TopMiddle")){
			return PositionInPanel.TopMiddle;
		}
		if (stringPosition.equals("BottomMiddle")){
			return PositionInPanel.BottomMiddle;
		} else {
			throw new UnrecognisedPositionError(stringPosition);
		}
		
		
	}**/
	
	public int getTopLeftX(){
		return LocationX;		
	}
	public int getTopLeftY(){
		return LocationY;		
	}
	public int getTopRightY(){
		return LocationY;		
	}
	public int getTopRightX(){
		return LocationX+SizeX;		
	}
	public int getBottomRightY(){
		return LocationY+SizeY;		
	}
	public int getBottomRightX(){
		return LocationX+SizeX;		
	}
	public int getBottomLeftY(){
		return LocationY+SizeY;		
	}
	public int getBottomLeftX(){
		return LocationX;		
	}
	public int getXLocationByString(String location){
		
		if (location.compareToIgnoreCase("TopLeft")==0){
			return getTopLeftX();
		}
		if (location.compareToIgnoreCase("TopRight")==0){
			return getTopRightX();
		}
		if (location.compareToIgnoreCase("BottomLeft")==0){
			return getBottomLeftX();
		}
		if (location.compareToIgnoreCase("BottomRight")==0){
			return getBottomRightX();
		} else {
			return 0;
		}
		
	}
public int getYLocationByString(String location){
		
		if (location.compareToIgnoreCase("TopLeft")==0){
			return getTopLeftY();
		}
		if (location.compareToIgnoreCase("TopRight")==0){
			return getTopRightY();
		}
		if (location.compareToIgnoreCase("BottomLeft")==0){
			return getBottomLeftY();
		}
		if (location.compareToIgnoreCase("BottomRight")==0){
			return getBottomRightY();
		} else {
			return 0;
		}
		
	}
}
