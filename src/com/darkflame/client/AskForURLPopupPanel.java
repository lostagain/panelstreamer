package com.darkflame.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AskForURLPopupPanel extends PopupPanel {
	
	AskForURLPopupPanel thisPopup = this;
	final VerticalPanel popUpStuff = new VerticalPanel();
	final TextBox userURL = new TextBox();		
	final  Button confirm = new Button("Confirm");
	final  Button cancel = new Button("Cancel");
	final HorizontalPanel confirm_or_cancel = new HorizontalPanel();
	
public AskForURLPopupPanel(){
	
	userURL.setWidth("300px");
	
	popUpStuff.add(new Label("Enter URL:"));
	popUpStuff.add(userURL);
	confirm_or_cancel.setSpacing(5);
	confirm_or_cancel.add(confirm);
	confirm_or_cancel.add(cancel);
	popUpStuff.add(confirm_or_cancel);
	
	cancel.addClickHandler(new ClickHandler(){
		public void onClick(ClickEvent event) {
		thisPopup.hide();
		}
	});
	
	confirm.addClickHandler(new ClickHandler(){

		public void onClick(ClickEvent event) {
			
			//Window.alert("loading file;"+userURL.getText().trim());
			
			//display loading message
			Label LoadingLab = new Label("Loading..");
			
			popUpStuff.add(LoadingLab);
			
			panelstreamer.ContainerPanel.reset();
			
			panelstreamer.loadPSCFile(userURL.getText().trim(),true);
			//if load is successfull, we add to the file list
			
			//remove label
			popUpStuff.remove(LoadingLab);
			
			thisPopup.hide();
		}
		
	});
	
	//Window.alert("creating popup");
	super.add(popUpStuff);
}
@Override
public void show(){	
	//Window.alert("showing popup");
	super.show();
	userURL.setText("");
}

}
