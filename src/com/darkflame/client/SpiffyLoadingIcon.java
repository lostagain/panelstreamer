package com.darkflame.client;

import java.awt.Color;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.SimplePanel;

public class SpiffyLoadingIcon extends SimplePanel {

	int width = 70;
	int height = 70;
	int padding = 3;
	Canvas icon = Canvas.createIfSupported();
	CanvasElement maine = icon.getCanvasElement();
	Context2d drawplane = maine.getContext2d();

	Timer clock;
	double ANG = -Math.PI / 2;
	boolean autoRun=true;
	
	int TotalUnitsToLoad=0;
	double StepSize =0;
	
	public SpiffyLoadingIcon(boolean autoRun) {
		this.autoRun = autoRun;
		
		if (icon == null) {
			return;
		}
		this.setSize((width+padding) + "px", (height+padding) + "px");
		icon.setWidth(width + "px");
		icon.setHeight(height + "px");
		icon.setCoordinateSpaceWidth(width);
		icon.setCoordinateSpaceHeight(height);

		this.setWidget(icon);
		
		clock = new Timer() {
			

			@Override
			public void run() {


				//Optimisation might be possible by drawing just a line and using no fill at all
				
				ANG = ANG + 0.1;
				drawplane.beginPath();
				drawplane.setFillStyle(CssColor.make(0, 0, 150));
				drawplane.setStrokeStyle(CssColor.make(0, 0, 150));
				drawplane.moveTo(width / 2, height / 2);
				drawplane.arc(width / 2, height / 2, width / 2, -Math.PI / 2,
						ANG, false);
				drawplane.lineTo(width / 2, height / 2);
				// drawplane.closePath();
				drawplane.stroke();
				drawplane.fill();

				if (ANG > (Math.PI * 2)-Math.PI / 2 ) {
					//clear and restart
					ANG = -Math.PI / 2;
					drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(), icon.getCoordinateSpaceHeight());
				}
			}

		};


	}

	@Override 
	public void onAttach(){
		
		 ANG = -Math.PI / 2;
		//clock.run();
		 if (autoRun){
		clock.scheduleRepeating(200);
		 }
		super.onAttach();
	}
	
	public void stopAnimation(){
		clock.cancel();
	}
	public void reset(){
		ANG = -Math.PI / 2;
		drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(), icon.getCoordinateSpaceHeight());
		
	}
	
	public void startAnimation(){
		 ANG = -Math.PI / 2;
		clock.cancel();
		clock.scheduleRepeating(200);
	}
	
	/** sets the total "units" to be loaded, ie, number of images, files etc **/
	public void setTotalUnits(int setTotalUnitsToLoad){
		TotalUnitsToLoad = setTotalUnitsToLoad;
		
		//calculate the step needed when advancing
		StepSize = (Math.PI * 2)/TotalUnitsToLoad;
		
		
	}
	
	public void stepClockForward(){
		
		
		ANG = ANG + StepSize;
		drawplane.beginPath();
		drawplane.setFillStyle(CssColor.make(0, 0, 150));
		drawplane.setStrokeStyle(CssColor.make(0, 0, 150));
		drawplane.moveTo(width / 2, height / 2);
		drawplane.arc(width / 2, height / 2, width / 2, -Math.PI / 2,
				ANG, false);
		drawplane.lineTo(width / 2, height / 2);
		// drawplane.closePath();
		drawplane.stroke();
		drawplane.fill();

		if (ANG > (Math.PI * 2)-Math.PI / 2 ) {
			//clear and restart
			ANG = -Math.PI / 2;
			drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(), icon.getCoordinateSpaceHeight());
		}
		
	}
	
}
