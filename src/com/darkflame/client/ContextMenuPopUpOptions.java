package com.darkflame.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.PopupPanel;

public class ContextMenuPopUpOptions extends PopupPanel {
	
	MenuBar contents = new MenuBar(true);
    
	public ContextMenuPopUpOptions(){
		
		this.getElement().getStyle().setZIndex(90000);
		
		
		 Command cmd = new Command() {
		      public void execute() {
		    	  
		        Window.alert("You selected a menu item!");
		        
		      }
		    };
		

		contents.addItem("Send Over", cmd);
		contents.addItem("Send Under", cmd);
		contents.addItem("menu", cmd);
		
		this.add(contents);
		
	}
}
