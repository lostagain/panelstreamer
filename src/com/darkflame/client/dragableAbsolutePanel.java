package com.darkflame.client;

import java.util.logging.Level;
import java.util.logging.Logger;


import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.dom.client.TouchCancelEvent;
import com.google.gwt.event.dom.client.TouchCancelHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FocusPanel;

public class dragableAbsolutePanel extends FocusPanel implements
		MouseDownHandler, MouseUpHandler, MouseMoveHandler, MouseWheelHandler,
		MouseOverHandler, MouseOutHandler, FocusHandler,
		TouchStartHandler,TouchMoveHandler,TouchEndHandler,TouchCancelHandler {

	
	AbsolutePanel Container = new AbsolutePanel();
	Boolean draging = false;
	Boolean isMoving = false;
	Boolean isCoasting = false;
	
	boolean XMOVEMENTDISABLED = false;
	boolean YMOVEMENTDISABLED = false;
	
	int dragStartX = 0, dragStartY = 0;
	int top = 0, left = 0;
	
	//ArrayList<AbsolutePanel> inner = new ArrayList<AbsolutePanel>();
	//ArrayList<layerInfo> innerinfo = new ArrayList<layerInfo>();

	// dragable panel array
	// ArrayList <AbsolutePanel> dragablePanels = new ArrayList
	// <AbsolutePanel>();

	int ContainerSizeX = 0, ContainerSizeY = 0;

	int locationdownx = 0;
	int locationdowny = 0;
	long dragstart = System.currentTimeMillis();
	Timer motionflow;
	//frame update time
	static final int FRAME_RATE_UPDATE_TIME = 50; //50 is safe 
	
	private double MotionDisX = 0;
	private double MotionDisY = 0;

	// The main layer that stores everything
	AbsolutePanel dragableLayer = new AbsolutePanel();

	private int CurrentMouseX = 0;
	private int CurrentMouseY = 0;
	private boolean hardStop = false;

	//messy - the max_height at the moment includes the subtraction for screen area, thus it can be negative
	private int Max_Height = -10000;// default
	private int Max_Width = -10000;// default
	
	//Logger

    Logger Log = Logger.getLogger("dragablePanelLog");
	private double StartDis=1;
	private double currentDis=1;
	private boolean justZoomed;
	/**
	 * if sizes given are bigger, then update the max size internally This
	 * should be called on any loading of new panels into the database or canvas
	 * remember x and y should include the widget size too, as they are measured
	 * from top left, and its bottom right thats the limit
	 **/
	public void updateMaxSize(int x, int y) {
		 Log.log(Level.SEVERE, "Test Log");

		 
		Log.info("updating size"+x+" y="+y);
		
		if (Max_Height < (y- ContainerSizeY)) {
			Max_Height = y - ContainerSizeY;
			Log.info("mh updated");
			}
		if (Max_Width < (x- ContainerSizeX)) {
			Max_Width = x - ContainerSizeX;
			Log.info("mw updated");
			}
		Log.info("Max. width for layers = " + Max_Height+":");
		Log.info("Max. height for layers = " + Max_Width+":");
		
	}
/** sets the size limits of panning (doesn't check there isn't out of range panels, unless your reseting/resizing you should use update)**/
	public void setMaxSize(int x,int  y){
		Max_Width = x;
		Max_Height = y;
		Log.info("resetting width and height for layers = " + x +" - "+y);
	}
	
	public int getMaxHeight(){
		Log.info("Max height ="+Max_Height+" cotainer = "+ContainerSizeY);
		return Max_Height+ContainerSizeY;
	}
	
	public int getMaxWidth(){
		Log.info("Max_Width ="+Max_Width+" cotainer = "+ContainerSizeX);
		return Max_Width+ContainerSizeX;
	}
	
	public dragableAbsolutePanel(int sizeX, int sizeY) {

		ContainerSizeX = sizeX;
		ContainerSizeY = sizeY;

		Container.setPixelSize(sizeX, sizeY);

		dragableLayer.getElement().getStyle().setBackgroundImage(
				"url(images/BACKGROUND.jpg)");
		
				
			
		this.add(Container);
		

		//using standards mode needs this line for it to display correctly
		Container.getElement().getStyle().setPosition(Position.ABSOLUTE);
		dragableLayer.getElement().getStyle().setPosition(Position.ABSOLUTE);
		
		Container.add(dragableLayer, 0, 0);

		
		// set to default size
		dragableLayer.setSize("10000px", "10000px");

		// AnotherFocusPanel.add(Container);
		// this.add(AnotherFocusPanel);

		//mouse
		this.addMouseMoveHandler(this);
		this.addMouseUpHandler(this);
		this.addMouseDownHandler(this);
		this.addMouseOutHandler(this);
		this.addMouseOverHandler(this);
		
		//touch
		this.addTouchStartHandler(this);
		this.addTouchEndHandler(this);
		this.addTouchMoveHandler(this);
		this.addTouchCancelHandler(this);
		
		//focus and wheel		
		this.addFocusHandler(this);
		this.addMouseWheelHandler(this);

		// AnotherFocusPanel.addMouseListener(this);
		// AnotherFocusPanel.addMouseWheelListener(this);

		// create motion flow
		motionflow = new MotionFlowTimer();

	}

	public void onFocus(FocusEvent event) {

		panelstreamer.MainControlBar.closeAllOpenWindows();

	}

	@Override
	public void setSize(String sizeX, String sizeY) {

		super.setSize(sizeX, sizeY);
		Container.setSize(sizeX, sizeY);

		ContainerSizeX = super.getOffsetWidth();
		ContainerSizeY = super.getOffsetHeight();

		// Container.setPixelSize(sizeX, sizeY);
	};

	public void reset() {
		stopAndReset();

		if (!dragableLayer.isAttached()) {
			Container.add(dragableLayer, 0, 0);
		}
	}

	// NEW method - all panels on same absolute panel!
	public void addPanel(ComicPanel newPanel, int x, int y) {

		Log.info("adding panels at..." + x + " y" + y);

		// add it to the container layer directly
		dragableLayer.add(newPanel, x, y);
		//set its parent to this
		newPanel.ParentPanel  = dragableLayer;
		
		// make canvas bigger if needed (and enlarge max height/width)
		Log.info("existingPanel.ScaledSizeX = "+newPanel.ScaledSizeX);
		Log.info("existingPanel.ScaledSizeY = "+newPanel.ScaledSizeY);
		Log.info("added panel  at ZIndex= " + newPanel.panelsZDepth);

		this.updateMaxSize(x + newPanel.ScaledSizeX, y + newPanel.ScaledSizeY);

		// (should already have correct zIndex based on its layer at creation)
		
	}

	// NEW method - all panels on same absolute panel!
	public void movePanel(ComicPanel existingPanel, int x, int y) {

		Log.info("moving panel ...");
		// add it to the container layer directly
		dragableLayer.setWidgetPosition(existingPanel, x, y);
		// make canvas bigger if needed (and enlarge max height/width)
		Log.warning("existingPanel.ScaledSizeX = "+existingPanel.ScaledSizeX);
		Log.warning("existingPanel.ScaledSizeY = "+existingPanel.ScaledSizeY);
		Log.warning("moving panel  at ZIndex= " + existingPanel.panelsZDepth);
		this.updateMaxSize(x + existingPanel.ScaledSizeX, y + existingPanel.ScaledSizeY);

		Log.info("updated size mh="+Max_Height);
		// (should already have correct zIndex based on its layer at creation)
		

	}

	private void stopAndReset() {
		Log.info(" - resetting - ");
		// stop motion
		if (motionflow != null) {
			motionflow.cancel();
		}

		// clear any existing
		Container.clear();
		//inner.clear();
		//innerinfo.clear();
		dragableLayer.clear();

		// reset pos vars etc.
		draging = false;
		isMoving = false;
		isCoasting = false;
		dragStartX = 0;
		dragStartY = 0;
		top = 0;
		left = 0;
		locationdownx = 0;
		locationdowny = 0;
		dragstart = System.currentTimeMillis();
		MotionDisX = 0;
		MotionDisY = 0;

		Max_Height = -10000;

		Max_Width = -10000;
	}



	// public void onMouseDown(Widget sender, int x, int y) {
	public void onMouseDown(MouseDownEvent event) {


		int x = event.getX();
		int y = event.getY();
		
		onMouseOrTouchDown(event.getNativeEvent(),x,y);

	}
	
	private void onMouseOrTouchDown(NativeEvent event,int x,int y) {
		
		if (event.getButton()==NativeEvent.BUTTON_LEFT){
		
			BottomBar.setMouseStatus("Mouse or Touch Down");
			
			
		if ((panelstreamer.EDITMODE == false || panelstreamer.PanelBeingMoved == false)) {
			// move the field of view
			DOM.setCapture(this.getElement());
		} else {
			// move the element
			// DOM.setCapture(this.getElement());
			return;

		}

		//int x = event.getX();
		//int y = event.getY();

		// test if anything is under mouse
		Log.info("drag start a " + x + " y=" + y);
		//			

		Log.info("mouse down..");

		draging = true;
		dragStartX = x - left;
		dragStartY = y - top;

		// to calculate speed we need to save the starting location and the
		// current time

		locationdownx = left;
		locationdowny = top;
		dragstart = System.currentTimeMillis();
		
		}
	}

	public void onMouseOver(MouseOverEvent event) {
		draging = false;
	}

	public void onMouseOut(MouseOutEvent event) {


		BottomBar.setMouseStatus("Mouse Out");

		if (draging == true) {
			draging = false;
		
			int sendXout = ((-left) + (((ContainerSizeX / 2))));
			int sendYout = ((-top) + (((ContainerSizeY / 2))));

			// now update the position		
			panelstreamer.UpdatePosition(sendXout, sendYout);

			// motion flow
			motionflow();
		}
	}

	public void onMouseMove(MouseMoveEvent event) {		

		// public void onMouseMove(Widget sender, int x, int y) {

		
		int dx = event.getRelativeX(dragableLayer.getElement());
		int dy = event.getRelativeY(dragableLayer.getElement());
		
		
		int x = event.getX();
		int y = event.getY();
				
		
		mouseOrTouch(dx,dy, x, y);
		
		
	}
	
	private void mouseOrTouch(int dx, int dy, int x, int y) {
		//detect panel being moved when its dragged outside itself
        if (panelstreamer.PanelBeingMoved) {
        	
        	//relative pos
    	
			//Log.info("mouse move detected outside of panel...");
        	//Log.info("_______mousemove detected outside of panel...x="+ x+"-"+panelstreamer.PanelBeingEdited.dragStartX+"="+(x - panelstreamer.PanelBeingEdited.dragStartX));
        	//Log.info("_______mousemove detected outside of panel...dx="+ dx+"-"+panelstreamer.PanelBeingEdited.dragStartX+"="+(dx - panelstreamer.PanelBeingEdited.dragStartX));
        
			panelstreamer.ContainerPanel.movePanel
					(
					panelstreamer.PanelBeingEdited, 
					dx - panelstreamer.PanelBeingEdited.dragStartX, 
					dy - panelstreamer.PanelBeingEdited.dragStartY
					);

			
			// end move			
			int cx =panelstreamer.PanelBeingEdited.getCurrentX();
			int cy=panelstreamer.PanelBeingEdited.getCurrentY();
			int cz = panelstreamer.PanelBeingEdited.getCurrentZ();
			//we just set the location, rather then all the data (quicker)
			panelstreamer.infoPanel.setLocationData(cx, cy, cz);
					
			//panelstreamer.infoPanel.setData(panelstreamer.PanelBeingEdited.getInfoData());
        }
		
		if (draging == true) {

			//BottomBar.setMouseStatus("Move While Dragging");
			
			Log.info("mouse moveing and dragging");

								
			if (!XMOVEMENTDISABLED){
				left = x - dragStartX;
			}
			if (!YMOVEMENTDISABLED) {
				top = y - dragStartY;
			};

			// make sure X/Y isn't outside boundaries
			if ((left > 0)&&(!XMOVEMENTDISABLED)) {
				left = 0;
			};
			if ((top > 0)&&(!YMOVEMENTDISABLED)) {
				top = 0;
			};

			Log.info("setting co-ordinates");
			setPositionInternalCoOrdinates(left, top);
		} else {

			//pointless updating the mouse relative location when dragging...it shouldn't change!
			
		CurrentMouseX = (int) ((x-left)/(panelstreamer.GlobalZoom / 100)); //relative to top left unscaled
		CurrentMouseY = (int)((y-top)/(panelstreamer.GlobalZoom / 100));

		panelstreamer.MainControlBar.setCurrentMousePositionLabel(
				CurrentMouseX, CurrentMouseY);
		}
	}

	private void displacebyInternalCoOrdinates(int disX, int disY) {

		// make sure X/Y isn't outside boundary's
		if ((left > 0)&&(!XMOVEMENTDISABLED)) {
			left = 0;
		};
		if ((top > 0)&&(!YMOVEMENTDISABLED)) {
			top = 0;
		};
		
		//stop movement if disabled		
		if (XMOVEMENTDISABLED){
			Log.info("______movement disabled in X");			
			disX=0; //no displacement
		}
		if (YMOVEMENTDISABLED){		
			Log.info("______movement disabled in Y");			
			disY=0;//no displacement
		}

		//if both disabled then we just stop
		
		
		
		// stop movement at bottom right limits
		if ((top < -Max_Height)&&(!YMOVEMENTDISABLED)) {
			Log.info("hit height:" + Max_Height);
			top = -Max_Height;
		};

		if ((left < -Max_Width)&&(!XMOVEMENTDISABLED)) {			
			Log.info("hit width" + Max_Width);
			left = -Max_Width;
		};

		// get new co-ordinates based on old ones
		left = left + disX;
		top = top + disY;

		Log.info("set co-ordinates to "+left+" "+top);
		setPositionInternalCoOrdinates(left, top);

	}

	private void setPositionInternalCoOrdinates(int setX, int setY) {

		
		// NEW: Move only the layer
		Container.setWidgetPosition(dragableLayer, setX, setY);
				
		Log.info("coordinates set");
		

	}

	public void setPosition(int setX, int setY) {

		Log.info("setting :" + setX + " " + setY);
		// change to internal co-ordinates;
		// (-left)+ContainerSizeX/2
		setX = (-(setX - (ContainerSizeX / 2)));
		setY = (-(setY - (ContainerSizeY / 2)));

		top = setY;
		left = setX;

		// NEW: Move the only layer!
		Container.setWidgetPosition(dragableLayer, setX, setY);
		Log.info("co-ordinates set :" + setX + " " + setY);

		
	}

	// public void onMouseUp(Widget sender, int x, int y) {
	public void onMouseUp(MouseUpEvent event) {
		BottomBar.setMouseStatus("Mouse Up");
		
		int x = event.getRelativeX(dragableLayer.getElement());
		int y = event.getRelativeY(dragableLayer.getElement());
		
		mouseOrTouchEnd(x,y);
	}
	
	
	private void mouseOrTouchEnd(int x,int y) {
		
		Log.severe("mouseup draga abs panel...");	
		
		if (panelstreamer.PanelBeingMoved) {

			
			// move it
			//int x = event.getRelativeX(dragableLayer.getElement());
			//int y = event.getRelativeY(dragableLayer.getElement());
			
			
			
			//Log.info("_______mouse up detected outside of panel...x="+ x+" "+panelstreamer.PanelBeingEdited.dragStartX);

			panelstreamer.ContainerPanel.movePanel(
					panelstreamer.PanelBeingEdited, x
							- panelstreamer.PanelBeingEdited.dragStartX, y
							- panelstreamer.PanelBeingEdited.dragStartY);

			// end move
			panelstreamer.infoPanel.setData(panelstreamer.PanelBeingEdited.getInfoData());
			
			panelstreamer.PanelBeingEdited.updateData(panelstreamer.PanelBeingEdited.getInfoData(),false);
			
			panelstreamer.PanelBeingMoved = false;
				//panelstreamer.PanelBeingEdited = null;
			return;

		} else {

			draging = false;
			// used to be "this"
			DOM.releaseCapture(this.getElement());
			// now update the position
			panelstreamer.UpdatePosition((-left) + (ContainerSizeX / 2), (-top)
					+ ContainerSizeY / 2);

			// to calculate speed we need to save the starting location and the
			// current time

			motionflow();
		}
	}

	/**
	 * This comment cancels all current movement and disables it till its
	 * reenabled
	 **/
	public void disableAllMovement() {

		// Log.info("disabled movement");
		if (isMoving) {
			motionflow.cancel();
		}
		isMoving = false;
		isCoasting = false;

		hardStop = true;

	}

	// reenables motionflowing
	public void enableAllMovement() {

		hardStop = false;

	}

	private void motionflow() {

		Log.info("set motionflow");

		int displacementX = left - locationdownx;
		int displacementY = top - locationdowny;
		long period = System.currentTimeMillis() - dragstart;
		// System.out.print("\n drag displacement time:"+period);

		// displacement per unit of time;
		MotionDisX = ((double) displacementX / (double) period) * 50;
		MotionDisY = ((double) displacementY / (double) period) * 50;

		Log.info("\n drag displacement:" + MotionDisX + " " + MotionDisY);

		// motionflow.cancel();
		// only start if not already running
		if (!(isCoasting)) {
			motionflow.scheduleRepeating(FRAME_RATE_UPDATE_TIME);
		} else {
			Log.info("\n already coasting, so no new motion flow needed!");
		}

	}

	// public void onMouseWheel(Widget sender, MouseWheelVelocity velocity) {
	public void onMouseWheel(MouseWheelEvent event) {

		// int velocity = event.getDeltaY();
		boolean update = false;

		if (event.isNorth()) {
			if (panelstreamer.GlobalZoom < 200) {
				panelstreamer.GlobalZoom = Math.round(panelstreamer.GlobalZoom
						* (1 + (Math.abs((double) event.getDeltaY()) / 10)));

				panelstreamer.ChangeInZoom = 100 * (1 + (Math
						.abs((double) event.getDeltaY()) / 10));
				
				System.out.print("\n" + panelstreamer.GlobalZoom);
				update = true;
			}
		}
		if (event.isSouth()) {
			if (panelstreamer.GlobalZoom > 5) {
				
				panelstreamer.GlobalZoom = Math.round(panelstreamer.GlobalZoom
						/ (1 + ((double) event.getDeltaY() / 10)));
				
				panelstreamer.ChangeInZoom = 100 / (1 + (Math
						.abs((double) event.getDeltaY()) / 10));

				System.out.print("\n" + panelstreamer.GlobalZoom);
				update = true;
			}
		}

		// get mouse

		if (update) {
			panelstreamer.ResizeAroundMouse(panelstreamer.GlobalZoom,
					CurrentMouseX, CurrentMouseY);
		}
		/*
		 * panelstreamer.ChangeInZoom = velocity.getDeltaY()*100;
		 * panelstreamer.GlobalZoom=panelstreamer.GlobalZoom *
		 * velocity.getDeltaY();
		 */

	}



	private final class MotionFlowTimer extends Timer {
		@Override
		public void run() {

			if (hardStop) {
				Log.info("hard stopping");
				isCoasting = false;
				motionflow.cancel();
				return;
			}

			// set in motion flag;
			isCoasting = true;

			Log.info("seting coordinates from timer:");

			displacebyInternalCoOrdinates((int) Math.round(MotionDisX),
					(int) Math.round(MotionDisY));

			// Log.info("set coordinates from timer_0");

			// slow down
			MotionDisX = (MotionDisX / 1.2);
			MotionDisY = (MotionDisY / 1.2);

			// stop
			isMoving = false;
			if ((MotionDisX < 1) && (MotionDisX > -1)) {
				MotionDisX = 0;
			} else {
				isMoving = true;
			}
			if ((MotionDisY < 1) && (MotionDisY > -1)) {
				MotionDisY = 0;
			} else {
				isMoving = true;
			}

			if (!(isMoving)) {
				this.cancel();
				Log.info("\n stoped");
				isCoasting = false;
				// one last update to trigger any things that only work when its
				// stoped;
				panelstreamer.UpdatePosition((-left) + (ContainerSizeX / 2),
						(-top) + ContainerSizeY / 2);

			}
			Log.info("set coordinates from timer_end");

		}
	}

	@Override
	public void onTouchCancel(TouchCancelEvent event) {
		
		BottomBar.setMouseStatus("Touch Cancel");
		
		//int x = event.getTouches().get(0).getRelativeX(dragableLayer.getElement());
		//int y = event.getTouches().get(0).getRelativeY(dragableLayer.getElement());

		event.preventDefault();
		
		//mouseOrTouchEnd(x,y);
		
	}
	@Override
	public void onTouchEnd(TouchEndEvent event) {

		int per = (int) ((currentDis/StartDis)*100);
		
		BottomBar.setMouseStatus("Touch End - "+(per));	
		
		if (justZoomed){
			
			panelstreamer.GlobalZoom=per;
			
			//only change if its changed over x amount?
			
			panelstreamer.ResizeAroundMouse(panelstreamer.GlobalZoom,
				CurrentMouseX, CurrentMouseY);
			
		}
		
		currentDis=1;
		StartDis=1;
		
		BottomBar.setNumofFingersDown(event.getTouches().length());
		
		int x = event.getTouches().get(0).getRelativeX(dragableLayer.getElement());
		int y = event.getTouches().get(0).getRelativeY(dragableLayer.getElement());

		event.preventDefault();
		
		mouseOrTouchEnd(x,y);
		
	}
	
	@Override
	public void onTouchMove(TouchMoveEvent event) {

		event.preventDefault();
		
		int fingers = event.getTouches().length();		
		BottomBar.setNumofFingersDown(fingers);
		
		if (fingers==2 && !panelstreamer.PanelBeingMoved){
			
			int difx =Math.abs(event.getTouches().get(0).getClientX() - event.getTouches().get(1).getClientX()); 
			int dify = Math.abs(event.getTouches().get(0).getClientY() - event.getTouches().get(1).getClientY()); 
			
			double dis = Math.hypot(difx, dify);
			currentDis = dis;
			
			int per = (int) ((currentDis/StartDis)*100);
			justZoomed=true;
			BottomBar.setMouseStatus("Touch Move distance="+per);
			
		} else {
			
			justZoomed=false;			
			currentDis=1;
			StartDis=1;
			BottomBar.setMouseStatus("Touch Move");
		}
		
		int dx = event.getTouches().get(0).getRelativeX(Container.getElement());
		int dy = event.getTouches().get(0).getRelativeY(Container.getElement());

		int x = event.getTouches().get(0).getRelativeX(event.getRelativeElement());
		int y = event.getTouches().get(0).getRelativeY(event.getRelativeElement());
		
		
		mouseOrTouch(dx,dy, x, y);
		
	}
	@Override
	public void onTouchStart(TouchStartEvent event) {

		
		int fingers = event.getTouches().length();
		
		BottomBar.setNumofFingersDown(fingers);
		
		if (fingers==2){
			
			int dx =Math.abs(event.getTouches().get(0).getClientX() - event.getTouches().get(1).getClientX()); 
			int dy = Math.abs(event.getTouches().get(0).getClientY() - event.getTouches().get(1).getClientY()); 
			
			double dis = Math.hypot(dx, dy);			

			StartDis = dis;
			
			
			BottomBar.setMouseStatus("Touch Start distance="+dis);
			
			
		} else {
			currentDis=1;
			StartDis=1;
			BottomBar.setMouseStatus("Touch Start");
		}

		int x = event.getTouches().get(0).getRelativeX(event.getRelativeElement());
		int y = event.getTouches().get(0).getRelativeY(event.getRelativeElement());
		
		event.preventDefault();
		
		onMouseOrTouchDown(event.getNativeEvent(),x,y);
		
		
	}
}
