package com.darkflame.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/** contains all the controls for the panelstreaming window **/
public class controllbar extends HorizontalPanel {

	//zoom controls
	 Button bigger = new Button("Zoom In");
	  Button smaller = new Button("Zoom Out");
	
	  final static Button EditFileXML = new Button("Edit XML");
	  final static ToggleButton Edit = new ToggleButton("Edit Mode Off","Edit Mode On");
	  
	  
	  //current mouse position
		 static Label lab_currentMouseX = new Label("-");	
		 static Label lab_currentMouseY = new Label("-");

		 static Label lab_currentDisX = new Label("-");	
		 static Label lab_currentDisY = new Label("-");
		 
		 //current canvas position
		 static Label lab_currentXpos = new Label("-");	
		 static Label lab_currentYpos = new Label("-");
		 
	  //file select place holder
		 static Label file_select_placeholder = new Label("Loading File list...");
		 TitleFileList FileListTitle;
		 
		 //layer list place holder
		 static Label layer_list_placeholder = new Label(" ");			
		 
		 //layer list
		 final static layerList LayerList = new layerList();
		  LayerListPopup LayerListPopupTitle;
		  
	public controllbar(){
		
		//make everything here unselectable
		this.addStyleName("unselectable");
		
		//fill container horizontal
		this.setWidth("100%");
		//set background to black
		
		//this.getElement().getStyle().setBackgroundColor("BLACK");
		this.getElement().getStyle().setColor("WHITE");
		this.getElement().getStyle().setZIndex(900000);
		
		addMouseFeedback();
		
		//set up zoom controls
		addZoomeControlls();
		
	 	this.add(layer_list_placeholder);
		this.setCellWidth(layer_list_placeholder, "200px");
		this.setCellHorizontalAlignment(layer_list_placeholder, ALIGN_CENTER);
		
		//add file select place holder
		this.add(file_select_placeholder);
		//layer list next
	
		//set up edit button
		addEditButtons();
		 
	
		 
	}

	public void setCurrentPositionLabel(int X,int Y){
		
		lab_currentXpos.setText("x = "+X+"");
	     lab_currentYpos.setText("y = "+Y+"");
	}
	
public void setCurrentMousePositionLabel(int X,int Y){
		
	lab_currentMouseX.setText("mx = "+X+"");
	lab_currentMouseY.setText("my = "+Y+"");
	}
	private void addMouseFeedback() {
		//add mouse feedback
		
		VerticalPanel mouseLoc = new VerticalPanel();
		
		mouseLoc.add(lab_currentMouseX);
		mouseLoc.add(lab_currentMouseY);
		
		lab_currentMouseX.setStylePrimaryName("FeedbackLabel");
		lab_currentMouseY.setStylePrimaryName("FeedbackLabel");
		
		mouseLoc.setWidth("64px");		
		this.add(mouseLoc);
		this.setCellWidth(mouseLoc, "65px");
		
		VerticalPanel canvasLoc = new VerticalPanel();
		canvasLoc.add(lab_currentXpos);
		canvasLoc.add(lab_currentYpos);
		canvasLoc.setWidth("64px");

		lab_currentXpos.setStylePrimaryName("FeedbackLabel");
		lab_currentYpos.setStylePrimaryName("FeedbackLabel");
		
		this.add(canvasLoc);
		this.setCellWidth(canvasLoc, "65px");
		
	}

	public void addFileSelectList(SpiffyListBox filelist){
		int PlaceholderInfex = this.getWidgetIndex(file_select_placeholder);
		
		FileListTitle = new TitleFileList(filelist);
		
		this.insert(FileListTitle, PlaceholderInfex);
		this.setCellVerticalAlignment(FileListTitle, HasVerticalAlignment.ALIGN_MIDDLE);
		file_select_placeholder.removeFromParent();
		
		
	}
	/** updates and displays the internal layer list. Should be fired file loading**/
	public void addLayerList(){
		int PlaceholderInfex = this.getWidgetIndex(layer_list_placeholder);
		
		LayerListPopupTitle = new LayerListPopup(LayerList);
		
		this.insert(LayerListPopupTitle, PlaceholderInfex);
		this.setCellHorizontalAlignment(LayerListPopupTitle, ALIGN_CENTER);
		this.setCellVerticalAlignment(LayerListPopupTitle, ALIGN_MIDDLE);
		
		layer_list_placeholder.removeFromParent();
		
		
	}
	
	public void closeAllOpenWindows(){
		LayerListPopupTitle.closePopUp();
		FileListTitle.closePopUp();
	}
	public void setTitle(String newTitle){
		FileListTitle.setTitle(newTitle);
		FileListTitle.closePopUp();
	}

	private void addEditButtons() {
	//	this.add(Edit);
	//	this.setCellVerticalAlignment(Edit, HorizontalPanel.ALIGN_MIDDLE);
		
		this.add(EditFileXML);
		EditFileXML.setWidth("90px");
		this.setCellWidth(EditFileXML, "100px");
		
		this.setCellVerticalAlignment(EditFileXML, HorizontalPanel.ALIGN_MIDDLE);
		
		this.setCellHorizontalAlignment(EditFileXML,HorizontalPanel.ALIGN_RIGHT );
		
		 EditFileXML.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event) {
					
					
					panelstreamer.EditBox.center();
				}
		  });
		 
		 Edit.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event) {
						
					
					
					panelstreamer.setEditMode(Edit.isDown());
					
					
				}
		  });
	}



	private void addZoomeControlls() {
		
		HorizontalPanel ZoomControlls = new HorizontalPanel();
		
		ZoomControlls.setVerticalAlignment(ALIGN_MIDDLE);
		ZoomControlls.add(smaller);
		ZoomControlls.add(bigger);	  
		
		smaller.addStyleName("ZoomButton");
		bigger.addStyleName("ZoomButton");
		
		  this.add(ZoomControlls);
		  this.setCellVerticalAlignment(ZoomControlls, ALIGN_MIDDLE);
		  this.setCellWidth(ZoomControlls,"150px");
		  
		  
		  smaller.addClickHandler(new ClickHandler(){

				public void onClick(ClickEvent event) {
				
					if (panelstreamer.GlobalZoom>5){				
						
						panelstreamer.GlobalZoom=panelstreamer.GlobalZoom/2;
						panelstreamer.ChangeInZoom = 50;			
					    panelstreamer.changeGlobalZoom(panelstreamer.GlobalZoom);
					    
					}
					
			}

			  
		  });
		  
		  bigger.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event) {
					
					if (panelstreamer.GlobalZoom<200){		
						
						panelstreamer.GlobalZoom=panelstreamer.GlobalZoom*2;
						panelstreamer.ChangeInZoom = 200;
						panelstreamer.changeGlobalZoom(panelstreamer.GlobalZoom);
						
				    }
			}
			  
		  });
	}
	
	
	
}
