package com.darkflame.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/** contains all the comic panels on a layer, and lets you show/hide them **/

public class ComicPanelLayer {

	ArrayList<ComicPanel> comicPanelsOnLayer = new ArrayList<ComicPanel>();
	
	Boolean isVisible = true;
	
	String Layername = "(noname)";
	//int zIndex=1000;

	static Logger Log = Logger.getLogger("_____ComicPanelLayer");
	
	public ComicPanelLayer(String name ){
		//this.zIndex = zIndex;
		this.Layername = name;
		
	}
	
	public void addPanel(ComicPanel newpanel){
		//set zdepth to match layers
		//newpanel.panelsZDepth = zIndex;
		Log.info("______adding panel "+newpanel.IDNumber+" to "+Layername);
		
		//set visibility
		newpanel.setVisible(isVisible);
		
		comicPanelsOnLayer.add(newpanel);
		
		
	}
	
	public void removePanel(ComicPanel newpanel){
		comicPanelsOnLayer.remove(newpanel);
	}
	
	public void setVisible(boolean visible){
		isVisible = visible;
		
		Iterator<ComicPanel> layersIt = comicPanelsOnLayer.iterator();
		
		while (layersIt.hasNext()){
			
		ComicPanel currentPanel = layersIt.next();
		
		currentPanel.setVisible(visible);
		Log.info("______setting panel "+currentPanel.IDNumber+" visible "+visible);
		
		
		}
		
	}
	
	public boolean isVisible(){
		return isVisible;
		
	}
	//Other functions
	//Set ZIndex (loops over all its panels setting the zIndex of them)
	
}
