package com.darkflame.client;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.user.client.ui.SimplePanel;


public class SpiffyBox extends SimplePanel {
	
	//this is a class to draw lines from one corner to another.
	
	//Canvas main = new Canvas();
	Canvas mainc = Canvas.createIfSupported();
	CanvasElement maine = mainc.getCanvasElement();
	Context2d main = maine.getContext2d();
	
	
	String Mode = "box";
	int width = 4;
	
	//constants
	static String boxMode = "box";
	static String elipseMode = "elipse";
	
	public SpiffyBox(int SizeX, int SizeY, String setMode){
		
		if (setMode.length()>1){
		Mode = setMode;
		}
		
		this.setPixelSize(SizeX,SizeY);
		mainc.setPixelSize(SizeX, SizeY);
		mainc.setCoordinateSpaceHeight(SizeY);
		mainc.setCoordinateSpaceWidth(SizeX);
	
		this.add(mainc);
		//main.setBackgroundColor(Canvas.TRANSPARENT);
		main.setStrokeStyle("Black");
		
		drawBox(SizeX, SizeY);
		
		
	}

	private void drawBox(int SizeX, int SizeY) {
		main.beginPath();
		
		width = (int)(3*(panelstreamer.GlobalZoom/100));
		main.setLineWidth(width);
	
		
		if (Mode.equals(boxMode)){
		main.rect(0, 0, SizeX, SizeY);
		}
		
		int mainwidth= mainc.getOffsetWidth(); //might be the wrong width/height
		int mainheight = mainc.getOffsetHeight();
		
		if (Mode.equals(elipseMode)){
			main.moveTo((mainwidth/2),(mainheight/2)+(mainheight/2.1) );
			int ang = 0;
			int x,y=0;
			while (ang <= 360){
				
				x = (int)(Math.sin(Math.toRadians(ang))*(mainwidth/2.1))+(mainwidth/2);
				y =  (int)(Math.cos(Math.toRadians(ang))*(mainheight/2.1))+ (mainheight/2);
				
				main.lineTo( x,y);
				
				//System.out.print("-----"+x+","+y);
				ang=ang+5;
				
			}
		}
		main.setFillStyle("White");
		main.fill();
		main.stroke();
	}
	
public void resize (int SizeX, int SizeY){
	this.setPixelSize(SizeX, SizeY);
//	main.setPixelSize(SizeX, SizeY);
	mainc.setPixelSize(SizeX, SizeY);
	mainc.setCoordinateSpaceHeight(SizeY);
	mainc.setCoordinateSpaceWidth(SizeX);

	drawBox(SizeX, SizeY);
}
}
