package com.darkflame.client.RichTextToolbar;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface RichTextImages extends ClientBundle {

	

	@Source("backColors.gif")	
	ImageResource backColors();
	
	@Source("bold.gif")	
	ImageResource bold();

	ImageResource createLink();

	ImageResource fonts();

	ImageResource fontSizes();

	ImageResource foreColors();

	ImageResource hr();

	@Source("I-con.png")
	ImageResource iCon();

	ImageResource indent();

	ImageResource insertImage();

	ImageResource italic();

	ImageResource justifyCenter();

	ImageResource justifyLeft();

	ImageResource justifyRight();

	ImageResource ol();

	ImageResource outdent();

	ImageResource remove();

	ImageResource removeFormat();

	ImageResource removeLink();

	ImageResource strikeThrough();

	ImageResource subscript();

	ImageResource superscript();

	ImageResource ul();

	ImageResource underline();

}
