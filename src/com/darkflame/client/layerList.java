package com.darkflame.client;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class layerList extends VerticalPanel {
	
	/** creates a vertical panel with togglable layers, for use with PanelStreamer **/
	
	public layerList(){
		this.setStyleName("whitetext");
		this.getElement().getStyle().setBackgroundColor("black");
		this.add(new Label("Layers:"));
				
		this.setWidth("150px");

		this.setBorderWidth(1);
	}
	
	public void setLayerList (ArrayList<layerInfo> LayerData){
		
		//clear old
		this.clear();
		this.add(new Label("Layers in file:"));
		
		int ID=0;
		Iterator<layerInfo> layersit = LayerData.iterator();
	while (layersit.hasNext()){
		
		layerInfo currentLayerinfo = layersit.next();
		layerOptions layersettings = new layerOptions(currentLayerinfo.Title,ID,true); 
		layersettings.setStyleName("whitetext");
		this.add(layersettings);
		ID++;
	}
	
	}
	
	
	public class layerOptions  extends HorizontalPanel {
		
		
		public layerOptions(String name,final int ID, boolean Visible){
			
			this.add(new Label(name));
			this.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
			
			CheckBox toggleThisLayer = new CheckBox("show");
			toggleThisLayer.setValue(Visible);
			
			toggleThisLayer.addClickHandler(new ClickHandler(){

				public void onClick(ClickEvent event) {
				
					panelstreamer.layers.get(ID).setVisible(!( panelstreamer.layers.get(ID).isVisible()) );
				
				}
				
			});
			
			this.add(toggleThisLayer);
			
			this.setSpacing(1);
			this.setWidth("100%");
			
			
		}
		
		
	}
		
		
		
		
	
	

}
