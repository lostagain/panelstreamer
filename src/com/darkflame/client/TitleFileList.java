package com.darkflame.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

/** A clickable title that triggers the dropdown filelist **/

public class TitleFileList extends Label {

	static String title = "Click here to select your file";
	PopupPanel filelistPopup;
	Label TITLE = this;
	
	boolean popupShowing=false;
	
	public TitleFileList(final SpiffyListBox popupContents){
		
		super(title);
		this.setStylePrimaryName("TitleLabel");
		this.setHeight("100%");
		
		//create popup
		filelistPopup = new PopupPanel();
		filelistPopup.add(popupContents);
		
		this.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				if (popupShowing==false){
				// open the filelist popup
				
				filelistPopup.setPopupPosition(TITLE.getAbsoluteLeft(), TITLE.getAbsoluteTop()+40);
				//filelistPopup.setWidth(TITLE.getOffsetWidth()+"px");
				filelistPopup.setAutoHideEnabled(true);
				filelistPopup.show();
				filelistPopup.getElement().getStyle().setZIndex(900001);
				filelistPopup.show();
				popupShowing=true;
			
				} else {
					filelistPopup.hide();
					popupShowing=false;
				}
			}
			
		});
		
	}
	
	public void setTitle(String newTitle){
		title=newTitle;
		this.setText(title);
	
	}
	
	public void closePopUp()
	{
		if (filelistPopup.isShowing()){
			filelistPopup.hide();	
			popupShowing=false;
		}
	
	}
	
}
