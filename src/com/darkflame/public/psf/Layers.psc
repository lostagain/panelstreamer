<Collection name="A test of the format">
<layer name="Basic" zIndex="1000">
<panel ID="3" Data="Thread=Col:White;FromID:14;Loc:BottomRight;ToID:5;Dest:TopLeft;"></panel>
<panel ID="4" Data="images/three.png" SizeX="298" SizeY="120" LocX="2250" LocY="1300" LocZ="0"></panel>
<panel ID="5" Data="images/four.png" SizeX="600" SizeY="1960" LocX="3950" LocY="1900" LocZ="0"></panel>
<panel ID="6" Data="images/five.png" SizeX="600" SizeY="1265" LocX="3952" LocY="3845" LocZ="1"></panel>
<panel ID="7" Data="images/six.png" SizeX="1270" SizeY="600" LocX="4455" LocY="4645" LocZ="1"></panel>
<panel ID="8" Data="images/sixb.png" SizeX="511" SizeY="271" LocX="5210" LocY="4835" LocZ="2"></panel>
<panel ID="9" Data="images/Seven.png" SizeX="1491" SizeY="430" LocX="5721" LocY="4860" LocZ="1"></panel>
<panel ID="10" Data="images/Sevenb.png" SizeX="132" SizeY="430" LocX="5721" LocY="4860" LocZ="2"></panel>
<panel ID="11" Data="images/eight.png" SizeX="1248" SizeY="1822" LocX="7179" LocY="3440" LocZ="0"></panel>
</layer>
<layer name="Text" zIndex="2000">
<panel ID="13" Data="images/one.png" SizeX="194" SizeY="190" LocX="700" LocY="590" LocZ="0"></panel>
<panel ID="14" Data="images/two.png" SizeX="300" SizeY="102" LocX="900" LocY="1050" LocZ="0"></panel>
<panel ID="16" Data="TextBox=Hello Welcome to the Panel Streaming Demo! \n Concept by Scott McCloud \n Created by Thomas Wrobel  " SizeX="350" SizeY="90" LocX="10" LocY="10" LocZ="0"></panel>
<panel ID="15" Data="images/text_four.png" SizeX="600" SizeY="1960" LocX="3950" LocY="1900" LocZ="0"></panel>
<panel ID="17" Data="images/text_five.png" SizeX="600" SizeY="1265" LocX="3952" LocY="3845" LocZ="0"></panel>
<panel ID="18" Data="TextBox= You can use the buttons at the \n top to zoom in and out" SizeX="320" SizeY="60" LocX="4840" LocY="4690" LocZ="0"></panel>
<panel ID="12" Data="images/6b_text.png" SizeX="511" SizeY="271" LocX="5210" LocY="4835" LocZ="0"></panel>
<panel ID="20" Data="images/Seven_text.png" SizeX="1491" SizeY="430" LocX="5721" LocY="4855" LocZ="0"></panel>
<panel ID="21" Data="images/Sevenb_TEXT.png" SizeX="132" SizeY="430" LocX="5721" LocY="4860" LocZ="3"></panel>
<panel ID="22" Data="images/eight_TEXT.png" SizeX="1248" SizeY="1822" LocX="7179" LocY="3440" LocZ="0"></panel>
</layer>
</Collection>